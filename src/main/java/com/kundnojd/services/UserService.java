package com.kundnojd.services;

import com.kundnojd.DTO.UserDTO;
import com.kundnojd.model.Answer;
import com.kundnojd.model.Survey;
import com.kundnojd.model.User;
import com.kundnojd.repository.AnswerRepository;
import com.kundnojd.repository.SurveyRepository;
import com.kundnojd.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private SurveyService surveyService;
    @Autowired
    private AnswerService answerService;
    @Autowired
    private AnswerRepository answerRepository;

    @Autowired
    private SurveyRepository surveyRepository;

    public Optional<User> getUser(Integer id) {
        return userRepository.findById(id);
    }

//    public List<User> getAllTechniciansByRealm(Realm realm) {
//        return userRepository.findAllByRealm(realm);
//    }

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    public User saveUser(User user) {
        return userRepository.save(user);
    }

//    public Double calculateTechnicianAverage(List<Survey> surveys) {
//        List<Double> technicianScores = scoresToList(surveys);
//
//        OptionalDouble averageScore = technicianScores.stream().mapToDouble(value -> value).average();
//        if (averageScore.isPresent()) {
//            return averageScore.getAsDouble();
//        }
//        return 0.0;
//    }

//    public List<Double> scoresToList(List<Survey> surveys) {
//        List<Double> technicianScores = new ArrayList<>();
//
//        for (Survey s : surveys) {
//            List<Answer> answers = answerService.getAllScoresBySurvey(s);
//            if (!answers.isEmpty()) {
//                if (!answerService.isNullInAnswer(answers)) {
//                    technicianScores.add(answerService.calculateAvg(answers));
//                }
//            }
//        }
//        return technicianScores;
//    }
//
//    public OptionalDouble calculateAverage(List<Double> scores) {
//        return scores.stream().mapToDouble(value -> value).average();
//    }
//
//    public Double calcuateAverageList(List<User> users) {
//        int noSurveyCount = 0;
//        double averages = 0;
//        for (User user : users) {
//            List<Survey> surveys = surveyService.getAllEvaluatedSurveysByTechnician(user, AnswerEnum.YES);
//            double temp = calculateTechnicianAverage(surveys);
//            if (temp > 0) {
//                averages += temp;
//            } else {
//                noSurveyCount++;
//            }
//        }
//        return averages / (users.size() - noSurveyCount);
//    }

    /**
     * Service that aims to take a list of technicians and dates. Within these conditions finding all YES-answered-surveys and do some math.
     * An arraylist is created and TechnicianDTO's are continuously added, for each technician in list.
     *
     * @param users A list of all technicians within the realm.
     * @param startDate start date of finding surveys
     * @param endDate end date of finding surveys
     * @return An ArrayList of TechnicianDTO, containing what's important for technician statistics.
     */
//    public List<UserDTO> createListDTO(List<User> users, LocalDate startDate, LocalDate endDate){
//        List<UserDTO> userDTOS = new ArrayList<>();
//        for (User user : users) {
//            /*List<Survey> surveys = surveyService.findAllBetweenDates(technician, startDate, endDate);
//           // List<Score> scores = scoreRepository.findAllBySurveyBetweenDatesAndTechnician(startDate, endDate, technician.getId());
//            double average = calculateTechnicianAverage(surveys);
//            //List<Survey> surveys = surveyService.getSurveysByScores(scores).stream().collect(Collectors.toList());
//            //List<Survey> surveys = new ArrayList<>();
//            //double average = scoreService.calculateAvg(scores);
//            int amountYesAnswered = surveyService.amountAnswerEnum(surveys, AnswerEnum.YES);
//            double total = average * amountYesAnswered;
//            technicianDTOS.add(new TechnicianDTO(
//                    technician.getId(),
//                    randomName(),
//                    surveys.size(),
//                    amountYesAnswered,
//                    average,
//                    (int)total
//            ));*/
//            userDTOS.add(new UserDTO(
//                    user.getId(),
//                    randomName(),
//                    surveyRepository.getUserCountTotalSurveyBetweenDates(startDate, endDate, user.getId()),
//                    surveyRepository.getUserCountAnsweredSurveyBetweenDates(startDate, endDate, user.getId(), 2),
//                    answerRepository.getUserAnswerAverageBetweenDates(startDate,endDate, user.getId()),
//                    3
//            ));
//        }
//        return userDTOS;
//    }

//    public String randomName(){
//        List<String> nameList = Arrays.asList("Stefan","Magnus","Anders","Patrick","Lena","Isabelle","Sandra");
//        Random random = new Random();
//        String randName = nameList.get(random.nextInt(nameList.size()));
//        return randName;
//    }

}
