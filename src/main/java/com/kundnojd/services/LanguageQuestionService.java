package com.kundnojd.services;

import com.kundnojd.DTO.LanguagesTextDTO;
import com.kundnojd.model.LanguageQuestion;
import com.kundnojd.model.Question;
import com.kundnojd.repository.LanguageQuestionRepository;
import com.kundnojd.util.MessageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LanguageQuestionService {
    @Autowired
    private LanguageQuestionRepository languageQuestionRepository;
    @Autowired
    private MessageUtil messageUtil;

    /**
     * Survey that aims to break up the content of the list LanguageTextDTO, while using the content to create a new LanguageQuestion
     * and trying to save it to the db.
     * @param list With the content: language-object and text-string.
     * @param question A question-object of which existing question_id the new text and language is added onto.
     * @return A response entity containing the {@link com.kundnojd.util.MessageUtil} which holds an explaining message through {@link com.kundnojd.model.CommonResponse}. Also the HttpStatus.
     */
    public ResponseEntity loopList(List<LanguagesTextDTO> list, Question question) {
        for (LanguagesTextDTO languageText : list) {
            try {
                languageQuestionRepository.save(new LanguageQuestion(languageText.getText(), languageText.getLanguage(), question));
            } catch (Exception e) {
                return messageUtil.responseEntity("Problem when adding question", HttpStatus.CONFLICT);
            }
        }
        return messageUtil.responseEntity("The language and question were added to question_id: " + question.getId(), HttpStatus.OK);
    }

    public List<LanguageQuestion> findLanguageQuestion(Question question) {
        return languageQuestionRepository.findAllByQuestion(question);
    }

    public Optional<LanguageQuestion> findLanguageQuestionById(Integer id) {
        return languageQuestionRepository.findById(id);
    }

    /**
     * A service that aims to use a languageQuestion object and check whether it contains a Language or a Text. If any are present,
     * they are supposed to become the new Language or Text in the DB. If that is the case, set/update the languageQuestionDB and return it.
     * @param languageQuestion Part of the request body, regarding which values to update.
     * @param languageQuestionDB The current languageQuestion found in the DB, which to be updated.
     * @return A freshly set and updated version of languageQuestionDB.
     */
    public LanguageQuestion setLanguageQuestion(LanguageQuestion languageQuestion, LanguageQuestion languageQuestionDB){
        if(languageQuestion.getLanguage() != null) {
            languageQuestionDB.setLanguage(languageQuestion.getLanguage());
        }
        if(languageQuestion.getText() != null ) {
            languageQuestionDB.setText(languageQuestion.getText());
        }
        return languageQuestionDB;
    }

    public ResponseEntity saveToDB(LanguageQuestion languageQuestion){
        ResponseEntity responseEntity;
        try {
            languageQuestionRepository.save(languageQuestion);
            responseEntity = messageUtil.responseEntity("The text and language was saved to DB on languageQuestionId: " + languageQuestion.getId(),HttpStatus.OK);
        } catch (Exception e) {
            responseEntity = messageUtil.responseEntity("Failed to save to DB", HttpStatus.CONFLICT);
        }
        return responseEntity;
    }
}




