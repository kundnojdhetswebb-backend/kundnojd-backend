package com.kundnojd.services;

import com.kundnojd.enums.AnswerEnum;
import com.kundnojd.enums.TimeSpanEnum;
import com.kundnojd.enums.WeekdaysEnum;
import com.kundnojd.DTO.ChildCalenderDTO;
import com.kundnojd.model.Survey;
import com.kundnojd.model.User;
import com.kundnojd.repository.SurveyRepository;
import com.kundnojd.util.MessageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.temporal.TemporalField;
import java.time.temporal.WeekFields;
import java.util.*;

@Service
public class SurveyService {
    @Autowired
    private SurveyRepository surveyRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private MessageUtil messageUtil;

    /**
     * Service for finding a specific survey by id.
     * @param id Id to find
     * @return An optional survey object, finding the specific survey
     */
    public Optional<Survey> getSurveyById(Integer id) {
        return surveyRepository.findById(id);
    }

    /**
     * Service for finding all surveys
     * @return A list of all surveys from the database
     */
    public List<Survey> getAllSurveys() {
        return surveyRepository.findAll();
    }

//    public List<Survey> getAllSurveysByTechnician(User user) {
//        return surveyRepository.findAllByTechnician(user);
//    }

    /**
     * Service for finding all surveys connected to a specific technician, while the survey also is answered as the param.
     * @param user The specific technician
     * @param answerEnum The specific enum to be compared
     * @return A list of survey objects which compares correct in the database repository
     */
//    public List<Survey> getAllEvaluatedSurveysByTechnician(User user, AnswerEnum answerEnum) {
//        return surveyRepository.findAllByTechnicianAndAndAnswer(user, answerEnum);
//    }

    /**
     * As a best praxis, all database communication is outsourced to services. Hence a saveToDB method - saving the incoming object to the db.
     * @param survey Object, which to save to database
     * @return A response entity containing the {@link com.kundnojd.util.MessageUtil} which holds data and an explaining message through {@link com.kundnojd.model.CommonResponse}. Also the HttpStatus.
     */
    public ResponseEntity saveToDB(Survey survey){
        ResponseEntity responseEntity;
        try {
            surveyRepository.save(survey);
            responseEntity = messageUtil.responseEntity(survey,"The survey has been saved to DB", HttpStatus.OK);
        } catch (Exception ex) {
            responseEntity = messageUtil.responseEntity("Something failed while saving survey", HttpStatus.CONFLICT);
        }
        return responseEntity;
    }

    /**
     * A service that saves a survey object to the database.
     * The survey object sets a default value of "answer" to PENDING, 0 in db.
     * @param userId Technician id of which the survey is specifically connected to
     * @return The survey object, which was saved to the db. If no technician was found, returns messageService with errormessage and status.
     */
    public Survey createSurvey(Integer userId) {
        Optional<User> user = userService.getUser(userId);
        Survey survey = new Survey(user.get());
        survey.setAnswer(AnswerEnum.PENDING);
        return surveyRepository.save(survey);
    }
    public Survey createSurveyAndLinkCustomer(Integer userId, Integer customerId, Integer surveyId) {
        Optional<User> user = userService.getUser(userId);
        Survey survey = new Survey(surveyId, user.get(),customerId);
        survey.setAnswer(AnswerEnum.PENDING);
        return surveyRepository.save(survey);
    }

    /**
     * A service that assists to set the object's answer to included param, and its date to current date.
     * @param surveyDB The object of which to set.
     * @param answer The enum of which to set the object to.
     * @return The object, freshly set with answer and date.
     */
    public Survey setEvaluatedAndDate(Survey surveyDB, AnswerEnum answer) {
        surveyDB.setAnswer(answer);
        surveyDB.setDate(LocalDate.now());
        return surveyDB;
    }

    /**
     * A service that assists to check whether a specific survey is set to a specific AnswerEnum.
     * @param survey The survey to compare
     * @param answer The Enum to include for comparison
     * @return returns True if Enum is same as survey's current.
     */
    public Boolean isSurveyAnswer(Survey survey, AnswerEnum answer) {
        return survey.getAnswer() == answer;
    }
//
//    public List<Survey> getSurveysByAfterDate(User user, LocalDate date) {
//        return surveyRepository.findAllByTechnicianAndDateIsAfter(user, date);
//    }

    /**
     * A service that will return all surveys between two periods of time, included in the queryDTO. The time-period is also checked through the TimeSpanEnum.
     * @param user Which technician of which the surveys will match.
     * @param queryDTO Always includes the startDate, while endDate is not necessary. For example when finding this_year and this_week.
     * @param timeSpanEnum A check for the Enums "Today" and "Yesterday" as these can be checked through jpa with just "..DateIs"
     * @return A list of surveys between two time-periods
     */
//    public List<Survey> getSurveysByAfterAndBeforeDate(User user, QueryDTO queryDTO, TimeSpanEnum timeSpanEnum) {
//        return queryDTO.getEndDate() != null ? // First condition
//                surveyRepository.findAllByTechnicianAndDateGreaterThanEqualAndDateLessThanEqual(
//                        user, queryDTO.getStartDate(), queryDTO.getEndDate())
//                : timeSpanEnum.equals(TimeSpanEnum.TODAY) || timeSpanEnum.equals(TimeSpanEnum.YESTERDAY) ? // Second condition
//                surveyRepository.findAllByTechnicianAndDateIs(user, queryDTO.getStartDate())
//                :
//                surveyRepository.findAllByTechnicianAndDateIsAfter( // If both first and second condition turns out false, this method is entered.
//                        user, queryDTO.getStartDate()) ;
//    }

    /**
     * TODO: Can be replaced with method "amountAnswerEnum"?
     * Inject a list of surveys and a specific answerEnum. If it matches, tick the counter.
     * @param surveys A list of which we compare
     * @param answerEnum Compare the list with this enum.
     * @return The number of how many surveys has a matching answer-state.
     */
//    public int getResponseRate(List<Survey> surveys, AnswerEnum answerEnum) {
//        int counter = 0;
//        for (Survey s : surveys) {
//            if (s.getAnswer() == answerEnum) {
//                counter++;
//            }
//        }
//        return counter;
//    }

    public List<ChildCalenderDTO> getWeekOfMonth(List<Survey> surveys, LocalDate date, TimeSpanEnum timeSpanEnum){
        Map<Integer, ChildCalenderDTO> weekOfMonth = new HashMap<>();
        List<ChildCalenderDTO> week = new ArrayList<>();
        for (Survey s : surveys) {

            date = LocalDate.of(s.getDate().getYear(),s.getDate().getMonth(),s.getDate().getDayOfMonth());
            TemporalField woy = WeekFields.of(Locale.getDefault()).weekOfWeekBasedYear();
//            Integer  weekOfYear  = switch (timeSpanEnum){
//                case TODAY, YESTERDAY, LAST_WEEK, THIS_WEEK, LATEST_WEEK -> null;
//                case LAST_MONTH, THIS_MONTH, LATEST_MONTH -> date.getDayOfWeek().getValue() == 7 ?
//                        date.isLeapYear() && date.get(woy)-1 == 0 ? 52 : date.get(woy)-1 : date.get(woy);
//                case LAST_YEAR, THIS_YEAR, LATEST_YEAR -> date.getMonthValue();
//                default -> null;
//            };
            Integer weekOfYear = 1;

            ChildCalenderDTO childCalenderDTO = weekOfMonth.containsKey(weekOfYear) ? weekOfMonth.get(weekOfYear) : new ChildCalenderDTO();
            if (s.getAnswer() == AnswerEnum.NO) {

                childCalenderDTO.setAnsweredNo(childCalenderDTO.getAnsweredNo()+1);
            }else if (s.getAnswer() == AnswerEnum.YES){
                childCalenderDTO.setAnsweredYes(childCalenderDTO.getAnsweredYes()+1);
            }

            weekOfMonth.put(weekOfYear, childCalenderDTO);
        }
        List<ChildCalenderDTO> childCalenderDTOS = new ArrayList<>();
        for (Map.Entry<Integer, ChildCalenderDTO> map : weekOfMonth.entrySet()) {
            childCalenderDTOS.add(new ChildCalenderDTO(map.getKey(),map.getValue().getAnsweredYes(),map.getValue().getAnsweredNo()));
        }
        return childCalenderDTOS;
    }


    // TODO this comment
    public List<ChildCalenderDTO> getDayOfWeek(List<Survey> surveys, LocalDate date) {

        Map<WeekdaysEnum, ChildCalenderDTO> weekdays = new HashMap<>();

        List<ChildCalenderDTO> days = new ArrayList<>();
        for (Survey s : surveys) {
            WeekdaysEnum weekday = WeekdaysEnum.valueOf(s.getDate().getDayOfWeek().name());
            ChildCalenderDTO childCalenderDTO = weekdays.containsKey(weekday) ? weekdays.get(weekday) : new ChildCalenderDTO();
            if (s.getAnswer() == AnswerEnum.NO) {

                childCalenderDTO.setAnsweredNo(childCalenderDTO.getAnsweredNo()+1);
            }else if (s.getAnswer() == AnswerEnum.YES){
                childCalenderDTO.setAnsweredYes(childCalenderDTO.getAnsweredYes()+1);
            }
            weekdays.put(weekday, childCalenderDTO);

        }
        List<ChildCalenderDTO> childCalenderDTOS = new ArrayList<>();

        // for (Map.Entry<WeekdaysEnum, ChildCalenderDTO> map : weekdays.entrySet()) {
        //     childCalenderDTOS.add(new ChildCalenderDTO(map.getKey(),map.getValue().getAnsweredYes(),map.getValue().getAnsweredNo()));
        // }

        return childCalenderDTOS;
    }
//    public List<Survey> findAllBetweenDates(User user, LocalDate startDate, LocalDate endDate){
//        return surveyRepository.findAllByTechnicianAndDateGreaterThanEqualAndDateLessThanEqual(user, startDate, endDate);
//    }
//    //TODO this overrides the getResponserate above?
//    public Integer amountAnswerEnum(List<Survey> surveys, AnswerEnum answerEnum){
//        return (int) surveys.stream().filter(survey -> survey.getAnswer() == answerEnum).count();
//    }
//    public List<Survey> findAllByTechnicianDatesAnswer(User user, LocalDate startDate, LocalDate endDate, AnswerEnum answerEnum){
//        return surveyRepository.findAllByTechnicianAndDateGreaterThanEqualAndDateLessThanEqualAndAnswer(user, startDate, endDate, answerEnum);
//    }

    public Survey getSurvey(Optional<Integer> caseId, User user, Integer customerId, AnswerEnum answerEnum){
        System.out.println(caseId.isPresent());
        return caseId.isPresent() ? new Survey(caseId.get(), user,customerId,answerEnum) : new Survey(user,customerId,answerEnum);
    }
//    public Set<Survey> getSurveysByScores(List<Answer> answers){
//        return answers.stream().map(score -> score.getSurvey()).collect(Collectors.toSet());
//    }
}
