package com.kundnojd.services;

import com.kundnojd.model.Moderator;
import com.kundnojd.repository.ModeratorRepository;
import com.kundnojd.util.MessageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ModeratorService {
    @Autowired
    private ModeratorRepository moderatorRepository;
    @Autowired
    private MessageUtil messageUtil;

    public List<Moderator> findAllModerators() {
        return moderatorRepository.findAll();
    }

    public Optional<Moderator> findModeratorById(Integer moderatorId) {
        return moderatorRepository.findById(moderatorId);
    }
    public ResponseEntity saveModerator(Moderator moderator){
        ResponseEntity responseEntity;
        try{
            responseEntity = messageUtil.responseEntity(moderatorRepository.save(moderator),"gut", HttpStatus.OK);
        } catch (Exception ex){
            responseEntity = messageUtil.responseEntity("Failed save moderator to db", HttpStatus.CONFLICT);
        }
        return responseEntity;
    }
    public Moderator setModerator(Moderator moderatorDB, Moderator moderator){
        if(moderator.getRealm() != null)
            moderatorDB.setRealm(moderator.getRealm());
        if(moderator.getPassword() != null)
            moderatorDB.setPassword(moderator.getPassword());
        if(moderator.getEmail() != null)
            moderatorDB.setEmail(moderator.getEmail());
        if(moderator.getAdmin() != null)
            moderatorDB.setAdmin(moderator.getAdmin());
        return moderatorDB;
    }

    public ResponseEntity<Moderator> deleteModerator(Moderator moderator){
        ResponseEntity<Moderator> responseEntity;
        try {
            moderatorRepository.delete(moderator);
            responseEntity = messageUtil.responseEntity(moderator, "Moderator has been deleted" , HttpStatus.OK);
        } catch (Exception ex){
            responseEntity = messageUtil.responseEntity(moderator, "Couldnt delete moderator",HttpStatus.CONFLICT);
        }
        return responseEntity;
    }
}
