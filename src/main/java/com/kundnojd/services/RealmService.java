package com.kundnojd.services;

import com.kundnojd.model.Realm;
import com.kundnojd.repository.RealmRepository;
import com.kundnojd.util.MessageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RealmService {

    @Autowired
    private RealmRepository realmRepository;
    @Autowired
    private MessageUtil messageUtil;

    public Optional<Realm> getRealm(Integer id){
        return realmRepository.findById(id);
    }

    public List<Realm> getAllRealms() {
        return realmRepository.findAll();
    }

    public ResponseEntity<Realm> createRealm(Realm realm) {
        ResponseEntity responseEntity;
        try{
            realmRepository.save(realm);
            responseEntity = messageUtil.responseEntity(realm,"Realm has been created and saved", HttpStatus.OK);
        }catch (Exception e){
            responseEntity = messageUtil.responseEntity(realm,"Something went wrong while saving Realm", HttpStatus.BAD_REQUEST);
        }
        return responseEntity;
    }
}
