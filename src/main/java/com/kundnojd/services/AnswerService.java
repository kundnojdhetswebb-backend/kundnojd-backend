package com.kundnojd.services;

import com.kundnojd.DTO.*;
import com.kundnojd.enums.CalendarTypeEnum;
import com.kundnojd.enums.TimeSpanEnum;
import com.kundnojd.model.*;
import com.kundnojd.repository.*;
import com.kundnojd.repository.IDTO.IUserAvgPeriod;
import com.kundnojd.util.MessageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.util.*;

@Service
public class AnswerService {

    @Autowired
    private AnswerRepository answerRepository;

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private SurveyRepository surveyRepository;
    @Autowired
    private MessageUtil messageUtil;

//    public List<ChildCalenderDTO> getCalendarEnumNumber(TimeSpanEnum timeSpan, LocalDate startDate, LocalDate endDate, Integer technicianId){
//        return switch (timeSpan){
//            case TODAY, YESTERDAY, LAST_WEEK, THIS_WEEK, LATEST_WEEK-> answerRepository.getAllChildCalenderDTOBetweenDatesAndUserIdWEEK(startDate, endDate, technicianId);
//            case LAST_MONTH, THIS_MONTH, LATEST_MONTH -> answerRepository.getAllChildCalenderDTOBetweenDatesAndUserIdMONTH(startDate, endDate, technicianId);
//            case LAST_YEAR, THIS_YEAR, LATEST_YEAR -> answerRepository.getAllChildCalenderDTOBetweenDatesAndUserIdYEAR(startDate, endDate, technicianId);
//        };
//    }

    public List<ChildCalenderDTO> getCalendarEnumNumber(TimeSpanEnum timeSpan, LocalDate startDate, LocalDate endDate, Integer technicianId) {
        List<ChildCalenderDTO> childCalenderDTOS;

        switch (timeSpan){
            case TODAY:
            case YESTERDAY:
            case LAST_WEEK:
            case LATEST_WEEK:
            case THIS_WEEK:
                childCalenderDTOS = answerRepository.getAllChildCalenderDTOBetweenDatesAndUserIdWEEK(startDate, endDate, technicianId);
                break;
            case LAST_MONTH:
            case LATEST_MONTH:
            case THIS_MONTH:
                childCalenderDTOS = answerRepository.getAllChildCalenderDTOBetweenDatesAndUserIdMONTH(startDate, endDate, technicianId);
                break;
            case LAST_YEAR:
            case LATEST_YEAR:
            case THIS_YEAR:
                childCalenderDTOS = answerRepository.getAllChildCalenderDTOBetweenDatesAndUserIdYEAR(startDate, endDate, technicianId);
                break;
            default:
                childCalenderDTOS = new ArrayList<>();
                break;
        }
        return childCalenderDTOS;
    }



//    public CalendarTypeEnum getCalendarEnumByTimeSpanEnum(TimeSpanEnum timeSpan){
//        return switch (timeSpan){
//            case TODAY, YESTERDAY -> CalendarTypeEnum.DAY;
//            case LAST_WEEK, THIS_WEEK, LATEST_WEEK-> CalendarTypeEnum.WEEK;
//            case LAST_MONTH, THIS_MONTH, LATEST_MONTH -> CalendarTypeEnum.MONTH;
//            case LAST_YEAR, THIS_YEAR, LATEST_YEAR -> CalendarTypeEnum.YEAR;
//        };
//    }
public CalendarTypeEnum getCalendarEnumByTimeSpanEnum(TimeSpanEnum timeSpan){
        CalendarTypeEnum calendarTypeEnum;
//        return switch (timeSpan){
//            case TODAY, YESTERDAY -> CalendarTypeEnum.DAY;
//            case LAST_WEEK, THIS_WEEK, LATEST_WEEK-> CalendarTypeEnum.WEEK;
//            case LAST_MONTH, THIS_MONTH, LATEST_MONTH -> CalendarTypeEnum.MONTH;
//            case LAST_YEAR, THIS_YEAR, LATEST_YEAR -> CalendarTypeEnum.YEAR;
//        };
        switch (timeSpan){
            case TODAY:
            case YESTERDAY:
                calendarTypeEnum = CalendarTypeEnum.DAY;
                break;
            case LAST_WEEK:
            case THIS_WEEK:
            case LATEST_WEEK:
            default:
                calendarTypeEnum = CalendarTypeEnum.WEEK;
                break;
            case LAST_MONTH:
            case THIS_MONTH:
            case LATEST_MONTH:
                calendarTypeEnum = CalendarTypeEnum.MONTH;
                break;
            case LAST_YEAR:
            case THIS_YEAR:
            case LATEST_YEAR:
                calendarTypeEnum = CalendarTypeEnum.YEAR;
                break;
        }
        return calendarTypeEnum;
    }

//    public QueryDTO getTimeSpan(TimeSpanEnum timeSpanEnum) {
//        LocalDate date = LocalDate.now();
//
//        return switch (timeSpanEnum) {
//            case TODAY -> new QueryDTO(date,date); // Today
//            case YESTERDAY -> new QueryDTO(date.minusDays(1), date.minusDays(1));
//            case LATEST_WEEK -> new QueryDTO(date.minusWeeks(1).plusDays(1), date); // One week from now
//            case LAST_WEEK -> new QueryDTO(lastWeek(date), lastWeek(date).plusWeeks(1).minusDays(1));
//            case LATEST_MONTH -> new QueryDTO(date.minusMonths(1), date);
//            case LAST_MONTH -> new QueryDTO(lastMonth(date), lastMonth(date).plusDays(lastMonth(date).lengthOfMonth()).minusDays(1));
//            case LATEST_YEAR -> new QueryDTO(date.minusYears(1), date);
//            case LAST_YEAR -> new QueryDTO(lastYear(date), lastYear(date).plusYears(1).minusDays(1));
//            case THIS_WEEK -> new QueryDTO(date.minusDays(date.getDayOfWeek().getValue()).plusDays(1), date);
//            case THIS_MONTH -> new QueryDTO(date.minusDays(date.getDayOfMonth()).plusDays(1), date);
//            case THIS_YEAR -> new QueryDTO(date.minusDays(date.getDayOfYear()).plusDays(1), date);
////            case THIS_YEAR -> new QueryDTO(date.minusDays(date.getDayOfYear()).plusDays(1), LocalDate.of(2021,12,31));
//        };

        public QueryDTO getTimeSpan(TimeSpanEnum timeSpanEnum) {
            LocalDate date = LocalDate.now();

            QueryDTO queryDTO;

            switch (timeSpanEnum){
                case TODAY:
                    queryDTO = new QueryDTO(date,date); // Today
                    break;
                case YESTERDAY:
                    queryDTO = new QueryDTO(date.minusDays(1), date.minusDays(1));
                    break;
                case LATEST_WEEK:
                    queryDTO = new QueryDTO(date.minusWeeks(1).plusDays(1), date); // One week from now
                    break;
                case LAST_WEEK:
                    queryDTO = new QueryDTO(lastWeek(date), lastWeek(date).plusWeeks(1).minusDays(1));
                    break;
                case LATEST_MONTH:
                    queryDTO = new QueryDTO(date.minusMonths(1), date);
                    break;
                case LAST_MONTH:
                    queryDTO = new QueryDTO(lastMonth(date), lastMonth(date).plusDays(lastMonth(date).lengthOfMonth()).minusDays(1));
                    break;
                case LATEST_YEAR:
                    queryDTO = new QueryDTO(date.minusYears(1), date);
                    break;
                case LAST_YEAR:
                    queryDTO = new QueryDTO(lastYear(date), lastYear(date).plusYears(1).minusDays(1));
                    break;
                case THIS_WEEK:
                    queryDTO = new QueryDTO(date.minusDays(date.getDayOfWeek().getValue()).plusDays(1), date);
                    break;
                case THIS_MONTH:
                    queryDTO = new QueryDTO(date.minusDays(date.getDayOfMonth()).plusDays(1), date);
                    break;
                case THIS_YEAR:
                    queryDTO = new QueryDTO(date.minusDays(date.getDayOfYear()).plusDays(1), date);
                    break;
                default:
                    queryDTO = new QueryDTO();
                    break;
            }
            return queryDTO;

    }
    private LocalDate lastWeek(LocalDate date){
        return date.minusWeeks(1).minusDays(date.getDayOfWeek().getValue());
    }
    private LocalDate lastMonth(LocalDate date){
        return LocalDate.of(date.getYear(), date.getMonth().minus(1), 1);
    }
    private LocalDate lastYear(LocalDate date){
        return LocalDate.of(date.getYear() - 1, 1, 1);
    }


    public List<Answer> createScoresList(List<Question> questions, Survey survey) {
        List<Answer> answers = new ArrayList<>();
        for (Question question : questions) {
            answers.add(updateAnswers(question,survey));
        }

        return answers;
    }

    public List<Answer> createScoresList(SurveyDTO surveyDTO) {
        List<Answer> answers = new ArrayList<>();
        System.out.println(surveyDTO.getQuestions().size());
        for (QuestionDTO QuestionDTO : surveyDTO.getQuestions()) {
            answers.add(updateAnswer(QuestionDTO, surveyDTO));
        }
        return answers;
    }

    private Answer updateAnswers(Question question, Survey survey) {
        return new Answer(question,survey);
    }

    private Answer updateAnswer(QuestionDTO QuestionDTO, SurveyDTO surveyDTO) {
        Answer answer = new Answer();
        if (QuestionDTO.getAnswerId() != null) {
            answer = answerRepository.findById(QuestionDTO.getAnswerId()).get();
        }

        Question question = questionRepository.findById(QuestionDTO.getQuestionId()).get();
        answer.setQuestion(question);
        answer.setSurvey(surveyDTO.getSurvey());

        if (QuestionDTO.getAnswerValue() != null) {
            answer.setValue(QuestionDTO.getAnswerValue());
        }
        return answer;

    }

    /**
     * A helping service that outsources the validation of the request body's score. Must be in range 1-10 to pass.
     * @param questions A list of QuestionDTOs.
     * @return A validating boolean
     */
    public Boolean isQuestionsAnswered(List<QuestionDTO> questions) {
        Boolean isAnswered = true;

        for (QuestionDTO q : questions) {
            if (q.getAnswerValue() == null || !(q.getAnswerValue() >= 1 && q.getAnswerValue() <= 10)) {
                isAnswered = false;
                break;
            }
        }
        return isAnswered;
    }


    public ResponseEntity saveConnectionToDB(Answer answer) {
        ResponseEntity responseEntity;
        try {
            answerRepository.save(answer);
            responseEntity = messageUtil.responseEntity(answer, "The question was synced to the survey", HttpStatus.OK);
        } catch (Exception ex) {
            responseEntity = messageUtil.responseEntity("Something failed while connecting score to question & survey", HttpStatus.CONFLICT);
        }
        return responseEntity;
    }

    public ResponseEntity saveToDB(List<Answer> answers, Survey survey) {
        ResponseEntity responseEntity;
        try {
            //Check ORDER!

            Survey surveyDB = surveyRepository.save(survey);
            List<Answer> answersDB = answerRepository.saveAll(answers);

            responseEntity = messageUtil.responseEntity(new SurveyResultsDTO(surveyDB, answersDB), "Survey was created.", HttpStatus.OK);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            responseEntity = messageUtil.responseEntity(survey, "ScoreServiceController Something failed while saving to db", HttpStatus.CONFLICT);
        }
        return responseEntity;
    }
    public List<IUserAvgPeriod> getUserAvgPeriods(TimeSpanEnum timeSpanEnum, List<Integer> userIds, LocalDate startDate, LocalDate endDate){

        List<IUserAvgPeriod> iUserAvgPeriods = new ArrayList<>();

        switch (timeSpanEnum){
            case THIS_WEEK:
            case LAST_WEEK:
            case LATEST_WEEK:
                iUserAvgPeriods = answerRepository.getAverageScoreDayByUsersAndBetweenDates(userIds, startDate, endDate);
                break;
            case THIS_MONTH:
            case LAST_MONTH:
            case LATEST_MONTH:
                iUserAvgPeriods = answerRepository.getAverageScoreWeekByUsersAndBetweenDates(userIds, startDate, endDate);
                break;
            case THIS_YEAR:
            case LAST_YEAR:
            case LATEST_YEAR:
                iUserAvgPeriods = answerRepository.getAverageScoreMonthByUsersAndBetweenDates(userIds, startDate, endDate);
                break;
            default:
                break;
        }
        // Java 14 syntax
//        return switch (timeSpanEnum){
//            case THIS_WEEK, LAST_WEEK, LATEST_WEEK -> answerRepository.getAverageScoreDayByUsersAndBetweenDates(userIds, startDate, endDate);
//            case LAST_MONTH, LATEST_MONTH, THIS_MONTH -> answerRepository.getAverageScoreWeekByUsersAndBetweenDates(userIds, startDate, endDate);
//            case LAST_YEAR, THIS_YEAR, LATEST_YEAR-> answerRepository.getAverageScoreMonthByUsersAndBetweenDates(userIds, startDate, endDate);
//            default -> throw new IllegalStateException("Unexpected value: " + timeSpanEnum);
//        };
        return iUserAvgPeriods;
    }

}