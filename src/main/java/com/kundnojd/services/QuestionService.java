package com.kundnojd.services;

import com.kundnojd.DTO.ModQuestionDTO;
import com.kundnojd.model.Question;
import com.kundnojd.model.Realm;
import com.kundnojd.model.Answer;
import com.kundnojd.model.Survey;
import com.kundnojd.repository.QuestionRepository;
import com.kundnojd.util.MessageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class QuestionService {
    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private MessageUtil messageUtil;

    @Autowired
    private AnswerService answerService;

    @Autowired
    private LanguageService languageService;

    public List<Question> getQuestions() {
        return questionRepository.findAll();
    }

    public Optional<Question> getQuestionById(Integer id) {
        return questionRepository.findById(id);
    }

    public ResponseEntity saveQuestion(Question question) {
        ResponseEntity responseEntity;
        try {
            responseEntity = messageUtil.responseEntity(questionRepository.save(question), "Question has been saved", HttpStatus.OK);
        } catch (Exception ex) {
            responseEntity = messageUtil.responseEntity("Couldnt update", HttpStatus.CONFLICT);
        }
        return responseEntity;
    }


    public ResponseEntity deleteQuestion(Integer id) {

        ResponseEntity responseEntity;
        try {
            questionRepository.deleteById(id);
            responseEntity = messageUtil.responseEntity("Question has been deleted", HttpStatus.OK);
        } catch (Exception ex){
            responseEntity = messageUtil.responseEntity("Couldnt delete question by id: " + id, HttpStatus.CONFLICT);
        }
        return responseEntity;
    }

//    public List<ModQuestionDTO> createModQuestions(List<Survey> surveys){
//        List<ModQuestionDTO> modQuestions = new ArrayList<>();
//        for (Survey survey: surveys) {
//            List<Answer> answers = answerService.getAllScoresBySurvey(survey);
//            for (Answer answer : answers) {
//                if(isQuestionId(modQuestions, answer.getQuestion().getId())){
//                    ModQuestionDTO modQuestionDTO = findQuestionDTO(modQuestions, answer.getQuestion().getId());
//                    int index = modQuestions.indexOf(modQuestionDTO);
//
//                    modQuestionDTO.setTotalScore(modQuestionDTO.getTotalScore() + answer.getValue());
//                    modQuestionDTO.setOccurancesSurvey(modQuestionDTO.getOccurancesSurvey() + 1);
//                    modQuestions.set(index, modQuestionDTO);
//                }
//                else{
//                    modQuestions.add(new ModQuestionDTO(answer.getQuestion().getId(), languageService.findLanguageNameByQuestionAndId(answer.getQuestion(), 1), answer.getValue()));
//                }
//            }
//
//        }
//        return modQuestions;
//    }

//    private Integer questionCount(List<Answer> answers, Question question){
//        return (int) answers.stream().filter(score -> score.getQuestion() == question).count();
//    }
//    private Boolean isQuestionId(List<ModQuestionDTO> questions, Integer id){
//        return questions.stream().anyMatch(question -> question.getId() == id);
//    }
//    private ModQuestionDTO findQuestionDTO(List<ModQuestionDTO> modQuestionDTOS, Integer id){
//        return modQuestionDTOS.stream().filter(question -> question.getId() == id).findFirst().get();
//    }

    public List<Question> getActiveQuestions(Realm realm) {
//        return questionRepository.findAllByRealmAndIsActive(realm,true);
        return new ArrayList<>();
    }

    public Question saveToDB(Question question) {
       return questionRepository.save(question);
    }

//    public Boolean existsById(Integer id) {
//        return questionRepository.existsById(id);
//    }

//    public Question setQuestionActive(Question questionDB){
//        questionDB.setActive(!questionDB.getActive());
//        return questionDB;
//    };
}
