package com.kundnojd.services;

import com.kundnojd.model.Language;
import com.kundnojd.model.LanguageQuestion;
import com.kundnojd.model.Question;
import com.kundnojd.model.Answer;
import com.kundnojd.repository.LanguageQuestionRepository;
import com.kundnojd.repository.LanguageRepository;
import com.kundnojd.util.MessageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import java.util.*;

@Service
public class LanguageService {
    @Autowired
    private LanguageRepository languageRepository;
    @Autowired
    private MessageUtil messageUtil;
    @Autowired
    private LanguageQuestionRepository languageQuestionRepository;

    public Optional<Language> getLanguageById(Integer id){
        return languageRepository.findById(id);
    }

    public Optional<Language> getLanguage(Optional<Integer> languageId, List<Language> languages) {
        Optional<Language> language = Optional.empty();

        if (languageId.isPresent()) {
            Optional<Language> language2 = languageRepository.findById(languageId.get());
            if (language2.isPresent()) {
                language = language2;
            }
        } else {
            language = sortLanguages(languages).stream().findFirst();
        }
        return language;
    }
    private List<Language> sortLanguages(List<Language> languageSet){
        //// --> sort language by ID
        List<Language> languageList = new ArrayList<>(languageSet);
        languageList.sort(Comparator.comparingInt(Language::getId));
        return languageList;
    }

//    public Set<Language> getAllLanguagesToQuestion(List<Answer> answers) {
//        Set<Language> languages = new HashSet<>();
//        for (Answer s : answers) {
//            List<LanguageQuestion> languageQuestionList = languageQuestionRepository.findAllByQuestion(s.getQuestion());
//            for (LanguageQuestion l : languageQuestionList) {
//                languages.add(l.getLanguage());
//            }
//        }
//        return languages;
//    }

//   public Set<Language> getAllLanguagesToQuestions(List<Question> questions) {
//        Set<Language> languages = new HashSet<>();
//        for (Question question : questions) {
//            List<LanguageQuestion> languageQuestionList = languageQuestionRepository.findAllByQuestion(question);
//            for (LanguageQuestion l : languageQuestionList) {
//                languages.add(l.getLanguage());
//            }
//        }
//        return languages;
//    }
//
//    public Set<Language> compareLanguages(Set<Language> languages, List<Answer> answers) {
//        Set<Language> languagesSet = new HashSet<>();
//        Map<String, Integer> languagesCounter = new HashMap<>();
//        for (Language l : languages) {
//            int tick = 0;
//            for (Answer s : answers) {
//                Optional<LanguageQuestion> langQuest = languageQuestionRepository.findByQuestionAndLanguage(s.getQuestion(), l);
//                if (langQuest.isPresent()) {
//                    tick ++;
//                    languagesCounter.put(l.getName(), tick);
//                }
//            }
//        }
//        for (Map.Entry<String, Integer> entry : languagesCounter.entrySet()) {
//            if(entry.getValue() == answers.size()){
//                languagesSet.add(languageRepository.findByName(entry.getKey()));
//            }
//        }
//        return languagesSet;
//    }
//    public String findLanguageNameByQuestionAndId(Question question, Integer languageId){
//        return languageQuestionRepository.findAllByQuestion(question).stream().filter(q -> q.getLanguage().getId() == languageId).findFirst().get().getText();
//    }

    public ResponseEntity saveLanguage(Language language) {
        ResponseEntity responseEntity;
        try {
            responseEntity = messageUtil.responseEntity(languageRepository.save(language),"The language has been saved to DB", HttpStatus.OK);
        } catch (Exception ex) {
            responseEntity = messageUtil.responseEntity("Something failed while saving language", HttpStatus.CONFLICT);
        }
        return responseEntity;
    }

    public List<Language> getAllLanguages() {
        return languageRepository.findAll();
    }

    public ResponseEntity deleteLanguage(Language language) {
        ResponseEntity responseEntity;
        try {
            languageRepository.delete(language);
            responseEntity = messageUtil.responseEntity(language, "Language has been deleted",HttpStatus.OK);
        }catch (Exception e){
            responseEntity = messageUtil.responseEntity("Couldnt delete the selected language", HttpStatus.CONFLICT);
        }
        return responseEntity;
    }

    public Optional<Language> findById(Integer id) {
        return languageRepository.findById(id);
    }
}
