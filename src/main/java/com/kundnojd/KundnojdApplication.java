package com.kundnojd;

import com.kundnojd.repository.RealmRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;

@SpringBootApplication
public class KundnojdApplication {

    public static void main(String[] args) {
        SpringApplication.run(KundnojdApplication.class, args);


    }
//    @Bean
//    public Docket productApi() {
//        return new Docket(DocumentationType.SWAGGER_2).select()
//                .apis(RequestHandlerSelectors.basePackage("com.kundnojd")).build();
//    }

}
