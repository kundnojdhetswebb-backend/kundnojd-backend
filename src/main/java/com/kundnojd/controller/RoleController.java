package com.kundnojd.controller;

import com.kundnojd.repository.RoleRepository;
import com.kundnojd.util.MessageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class RoleController {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private MessageUtil messageUtil;

//    @GetMapping("/roles")
//    public ResponseEntity getAllRoles(){
//        return messageUtil.responseEntity(roleRepository.findAll(), "All roles", HttpStatus.OK);
//    }
}
