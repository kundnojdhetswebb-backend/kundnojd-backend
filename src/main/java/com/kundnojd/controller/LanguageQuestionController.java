package com.kundnojd.controller;

import com.kundnojd.model.LanguageQuestion;
import com.kundnojd.services.LanguageQuestionService;
import com.kundnojd.util.MessageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api")
public class LanguageQuestionController {

    @Autowired
    private LanguageQuestionService languageQuestionService;

    @Autowired
    private MessageUtil messageUtil;

    /**
     * An API that aims to change a questions' text and the language_id it is connected to.
     * Requires the languageQuestion-object to include an id, of which to change.
     * If valid LanguageQuestion-id, include a new language which to be set, and the text. Both are optional.
     * Example:
     * {
     *     "id": 121,
     *     "language": {
     *         "id": 31
     *     },
     *     "text": "New question in new language!"
     * }
     *
     * @param languageQuestion An object containing what's needed to update the languageQuestion
     * @return A call to the service to saveToDB, thereby return as response entity
     */
    @PutMapping("/languageQuestion")
    public ResponseEntity changeLanguageQuestion(@RequestBody LanguageQuestion languageQuestion) {
        Optional<LanguageQuestion> languageQuestionDB = languageQuestionService.findLanguageQuestionById(languageQuestion.getId());
        if(languageQuestionDB.isEmpty()){
               return messageUtil.responseEntity("LanguageQuestion on Id: " + languageQuestion.getId() + " was not found", HttpStatus.CONFLICT);
        }
        LanguageQuestion languageQuestionUpdated = languageQuestionService.setLanguageQuestion(languageQuestion, languageQuestionDB.get());
        return languageQuestionService.saveToDB(languageQuestionUpdated);
    }
}
