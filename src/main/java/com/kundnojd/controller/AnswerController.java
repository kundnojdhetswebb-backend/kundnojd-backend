package com.kundnojd.controller;

import com.kundnojd.enums.AnswerEnum;
import com.kundnojd.enums.CalendarTypeEnum;
import com.kundnojd.enums.TimeSpanEnum;
import com.kundnojd.DTO.*;
import com.kundnojd.model.*;
import com.kundnojd.repository.AnswerRepository;
import com.kundnojd.repository.ParticipantRepository;
import com.kundnojd.services.*;
import com.kundnojd.util.DateUtil;
import com.kundnojd.util.MessageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class AnswerController {

    @Autowired
    private AnswerService answerService;
    @Autowired
    private SurveyService surveyService;
    @Autowired
    private QuestionService questionService;
    @Autowired
    private MessageUtil messageUtil;
    @Autowired
    private UserService userService;

    @Autowired
    private AnswerRepository answerRepository;

    @Autowired
    private DateUtil dateUtil;

    @Autowired
    private ParticipantRepository participantRepository;

    @PutMapping("/answernew")
    public ResponseEntity createAnswers(@RequestBody SurveyDTO surveyDTO, @RequestParam Optional<Integer> caseId) {
        System.out.println(surveyDTO.getUserId());

        Boolean questionsIsAnswered = answerService.isQuestionsAnswered(surveyDTO.getQuestions());
        if (!questionsIsAnswered) {
            return messageUtil.responseEntity("All questions were not answered correctly!", HttpStatus.CONFLICT);
        }
        Optional<User> user = userService.getUser(surveyDTO.getUserId());

        Survey survey = surveyService.getSurvey(caseId,user.get(), surveyDTO.getParticipantId(), AnswerEnum.YES);
        surveyDTO.setSurvey(survey);
        survey.setParticipant(participantRepository.getOne(surveyDTO.getParticipantId()));

        List<Answer> answers = answerService.createScoresList(surveyDTO);
        return answerService.saveToDB(answers,survey);

    }
    @PutMapping("/answer")
    public  ResponseEntity createNewAnswers(@RequestBody SurveyDTO surveyDTO){
        if(!answerService.isQuestionsAnswered(surveyDTO.getQuestions()))
            return messageUtil.responseEntity("All questions were not answered correctly!", HttpStatus.CONFLICT);
        Optional<Survey> survey = surveyService.getSurveyById(surveyDTO.getSurvey().getId());
        if(survey.isEmpty())
            return messageUtil.responseEntity("Couldnt find any survey", HttpStatus.NOT_FOUND);

        Survey surveyDB = survey.get();
        if(surveyDB.getAnswer() != AnswerEnum.PENDING)
            return messageUtil.responseEntity("Survey has already been answered", HttpStatus.CONFLICT);

        surveyDB.setDate(LocalDate.now());
        surveyDB.setAnswer(AnswerEnum.YES);

        surveyDTO.setSurvey(surveyDB);
        List<Answer> answers = answerService.createScoresList(surveyDTO);
        return answerService.saveToDB(answers, survey.get());

    }

    /**
     * This API aims to update the value of a survey's score. When a survey is updated correctly, the survey sets the AnswerEnum to YES.
     * The updated score must be in range 1-10, for {@link com.kundnojd.services.AnswerService} isQuestionsAnswered to pass.
     * @param surveyDTO consists of a survey object and a list of questionsDTO-objects.
     * @return A call to {@link com.kundnojd.services.AnswerService} to save the updated scores to the DB.
     */


    /**
     * An API that aims to present a technicians' statistics in between a specific time span.
     * Small math is done in the method, in terms of presenting the data, on request from frontend
     *
     * @param timeSpan Is used as a {@link com.kundnojd.enums.TimeSpanEnum}, it has to exactly match (THIS_MONTH, LATEST_WEEK, etc..)
     * @param id Id of technician in question.
     * @return A response entity containing the {@link com.kundnojd.util.MessageUtil} which holds data (as a weekDTO, filled with necessary statistics) and an explaining message through {@link com.kundnojd.model.CommonResponse}. Also the HttpStatus.
     */
    @GetMapping("/useranswerresponse/{id}")
    public ResponseEntity getAnswerResponse(
            @RequestParam Optional<String> timeSpan,
            @RequestParam Optional<String> startDate,
            @RequestParam Optional<String> endDate,
            @PathVariable("id") Integer id) {

        TimeSpanEnum time = dateUtil.getTimeSpan(timeSpan);

        QueryDTO date = dateUtil.getStartAndEndDate(startDate,endDate,timeSpan);


        System.out.println(date.getStartDate() + " " + date.getEndDate());

        Optional<ParentCalenderDTO> parentCalenderDTO = answerRepository.getAvgAnswerAndTotalSumByUserIdAndBetweenDates(id, date.getStartDate(), date.getEndDate());
        List<ChildCalenderDTO> childrens = answerService.getCalendarEnumNumber(time, date.getStartDate(), date.getEndDate(), id);
        Optional<Double> avgResponseRate = answerRepository.getAverageResponseRateByUserIdAndBetweenDates(id, date.getStartDate(), date.getEndDate());
        CalendarTypeEnum calendarType = answerService.getCalendarEnumByTimeSpanEnum(time);

        return messageUtil.responseEntity(new ParentCalenderDTO(
                parentCalenderDTO.get().getTotalScore(),
                parentCalenderDTO.get().getAvgScore(),
                avgResponseRate.get(),
                childrens,
                calendarType

        ), "Ok", HttpStatus.OK);

    }
}
