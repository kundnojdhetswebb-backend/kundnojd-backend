package com.kundnojd.controller;

import com.kundnojd.model.*;
import com.kundnojd.repository.*;
import com.kundnojd.services.LanguageService;
import com.kundnojd.services.QuestionService;
import com.kundnojd.util.MessageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/api")
public class LanguageController {

    @Autowired
    private LanguageService languageService;
    @Autowired
    private MessageUtil messageUtil;
    @Autowired
    private SurveyRepository surveyRepository;
    @Autowired
    private LanguageRepository languageRepository;

    @GetMapping("/language")
    public ResponseEntity getLanguages(){
        return messageUtil.responseEntity(languageService.getAllLanguages(),"All languages", HttpStatus.OK);
    }

    @GetMapping("/language/{id}")
    public ResponseEntity getLanguageById(@PathVariable("id")Integer id){
        Optional<Language> language = languageService.findById(id);

        return language.isPresent() ? messageUtil.responseEntity(language.get(),"Language found",HttpStatus.OK) :
                messageUtil.responseEntity("Language not found",HttpStatus.NOT_FOUND);
    }

    @PostMapping("/language")
    public ResponseEntity createLanguage(@RequestBody Language language){
        return languageService.saveLanguage(language);
    }

    @DeleteMapping("/language/{id}")
    public ResponseEntity deleteLanguage(@PathVariable("id") Integer id){
        Optional<Language> language = languageService.getLanguageById(id);
        if (language.isEmpty()){
            return messageUtil.responseEntity("There is no languge with that ID", HttpStatus.NOT_FOUND);
        }
        return languageService.deleteLanguage(language.get());
    }
    @GetMapping("/languages/survey/{surveyId}")
    public ResponseEntity getLanguagesBySurvey(@PathVariable("surveyId") Integer surveyId){
        Optional<Survey> survey = surveyRepository.findById(surveyId);
        return survey.isPresent() ?
                messageUtil.responseEntity(languageRepository.getAllLanguagesByRoleId(survey.get().getParticipant().getRole().getId())
                        , "All languages", HttpStatus.OK) :
                messageUtil.responseEntity("Survey not found", HttpStatus.NOT_FOUND);
    }
}
