package com.kundnojd.controller;

import com.kundnojd.model.Moderator;
import com.kundnojd.services.ModeratorService;
import com.kundnojd.util.MessageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.*;

@RestController
@RequestMapping("/api")
public class ModeratorController {

    @Autowired
    private ModeratorService moderatorService;

    @Autowired
    private MessageUtil messageUtil;

    /**
     * An API that returns all moderators in the DB.
     * @return A response entity containing the {@link com.kundnojd.util.MessageUtil} which holds data (List<Moderator>) and an explaining message through {@link com.kundnojd.model.CommonResponse}. Also the HttpStatus.
     */
    @GetMapping("/moderators")
    public ResponseEntity getModerators(){
        return messageUtil.responseEntity(moderatorService.findAllModerators(), "All moderators", HttpStatus.OK);
    }
    @GetMapping("/moderator/{id}")
    public ResponseEntity getModerator(@PathVariable("id") Integer id){
        Optional<Moderator> moderatorDB = moderatorService.findModeratorById(id);
        if(moderatorDB.isEmpty())
            return messageUtil.responseEntity("No moderator on ID: "+ id, HttpStatus.CONFLICT);

        return messageUtil.responseEntity(moderatorDB,"Moderator got'", HttpStatus.OK);
    }
    @PutMapping("/moderator")
    public ResponseEntity<Moderator> updateModerator(@RequestBody Moderator moderator){

        Optional<Moderator> findModerator = moderatorService.findModeratorById(moderator.getId());
        if(!findModerator.isPresent())
            return new ResponseEntity<>(moderator, HttpStatus.CONFLICT);

        Moderator moderatorDB = moderatorService.setModerator(findModerator.get(), moderator);

        return moderatorService.saveModerator(moderatorDB);
    }

    /**
     * {
     *     "id": 112,
     *     "password": 123,
     *     "email": "rugge@zoom.com",
     *     "realm": {
     *         "id": 1
     *     },
     *     "admin": true
     * }
     *
     * @param moderator
     * @return
     */
    @PostMapping("/moderator")
    public ResponseEntity createModerator(@RequestBody Moderator moderator){
        return moderatorService.saveModerator(moderator);
    }

    @DeleteMapping("/moderator")
    public ResponseEntity<Moderator> deleteModerator(@RequestBody Moderator moderator){
        ResponseEntity<Moderator> responseEntity;

        Optional<Moderator> findModerator = moderatorService.findModeratorById(moderator.getId());
        if(findModerator.isPresent())
            responseEntity = moderatorService.deleteModerator(findModerator.get());
        else
            responseEntity = new ResponseEntity<Moderator>(moderator, HttpStatus.NOT_FOUND);
        return responseEntity;
    }

}
