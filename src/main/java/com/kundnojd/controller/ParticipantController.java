package com.kundnojd.controller;

import com.kundnojd.repository.ParticipantRepository;
import com.kundnojd.util.MessageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ParticipantController {
    @Autowired
    private MessageUtil messageUtil;

    @Autowired
    private ParticipantRepository participantRepository;

//    @GetMapping("/participants")
//    public ResponseEntity getAllParticipants(){
//        return messageUtil.responseEntity(participantRepository.findAll(), "All participants", HttpStatus.OK);
//    }
}
