package com.kundnojd.controller;

import com.kundnojd.DTO.QueryDTO;
import com.kundnojd.enums.TimeSpanEnum;
import com.kundnojd.repository.AnswerRepository;
import com.kundnojd.repository.UserRepository;
import com.kundnojd.services.*;
import com.kundnojd.util.MessageUtil;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Slice;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.time.LocalDate;
import java.util.Optional;
import java.util.List;

@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    private AnswerService answerService;
    @Autowired
    private UserService userService;
    @Autowired
    private RealmService realmService;
    @Autowired
    private MessageUtil messageUtil;
    @Autowired
    private AnswerRepository answerRepository;

//    @Autowired
//    private Keycloak keycloak;

    @Autowired
    private UserRepository userRepository;
//    @GetMapping("/users")
//    public ResponseEntity getAll(){
//        return messageUtil.responseEntity(userService.getAllUsers(), "All tech received!", HttpStatus.OK);
//    }
//
//    @PostMapping("/user")
//    public ResponseEntity createUser(@RequestBody User user){
//
//        return messageUtil.responseEntity(userService.saveUser(user), "Technician was posted", HttpStatus.CREATED);
//    }
//
//    @GetMapping("/avguser/{id}")
//    public ResponseEntity getAverageForUser(@PathVariable("id") Integer id) {
//        Optional<User> user = userService.getUser(id);
//        if (user.isEmpty()) {
//            return messageUtil.responseEntity("No technician on ID" + id + " present", HttpStatus.NOT_FOUND);
//        }
//
//        LocalDate startDate = LocalDate.ofYearDay(2021, 1);
//        LocalDate endDate = LocalDate.ofYearDay(2021, 365);
//        return messageUtil.responseEntity(answerRepository.getUserAnswerAverageBetweenDates(startDate, endDate, user.get().getId()), "Here pls", HttpStatus.OK);
//    }
//
//    @GetMapping("/avgusersbyrealm/{id}")
//    public ResponseEntity getAverageForUsersRealm(@PathVariable("id") Integer id){
//        Optional<Realm> realm = realmService.getRealm(id);
//        if(realm.isEmpty()){
//            return messageUtil.responseEntity("No realm present for technician on id: "+ id, HttpStatus.NOT_FOUND);
//        }
//
//        Optional<Double> averages = answerRepository.getAverageAnswerByRealmId(id);
//
//        return averages.isPresent() ? messageUtil.responseEntity(averages.get(), "The avg by realm", HttpStatus.OK): messageUtil.responseEntity("Couldnt find any realm", HttpStatus.NOT_FOUND);
//    }

    /**
     * API that aims to give full score-statistics of all technicians within a specific realm, and dates.
     *
     * @param realmId Realm of interest
     * @param startDate Find survey from start date
     * @param endDate Find survey until end date
     * @return A response entity containing the {@link com.kundnojd.util.MessageUtil} which holds data (technicianDTOS) and an explaining message through {@link com.kundnojd.model.CommonResponse}. Also the HttpStatus.
     */

    @GetMapping("/usersbyrealm/{id}")
    public ResponseEntity getUsersByRealm(@PathVariable("id") Integer realmId,
                                                @RequestParam Optional<String> startDate,
                                                @RequestParam Optional<String> endDate,
                                                @RequestParam Optional<String> timeSpan,
                                                @RequestParam Integer page,
                                                @RequestParam Integer pageAmount){
        TimeSpanEnum time = timeSpan.isPresent() ? TimeSpanEnum.valueOf(timeSpan.get()) : TimeSpanEnum.THIS_WEEK;

        QueryDTO date = startDate.isPresent() && endDate.isPresent() && timeSpan.isEmpty() ? new QueryDTO(LocalDate.parse(startDate.get()), LocalDate.parse(endDate.get())) :
                answerService.getTimeSpan(time);

        try {
            Slice s = answerRepository.getModQuestionsByRealmIdAndBetweenDatesPage(realmId, date.getStartDate(),  date.getEndDate(), PageRequest.of(page, pageAmount));
            return messageUtil.pageResponseEntityOK(s, "Data for realm");
        } catch (Exception ex){
            return messageUtil.responseEntity("Server error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/usersaverages")
    public ResponseEntity getUserAveragePeriod(@RequestParam List<Integer> userIds,
                                               @RequestParam Optional<String> startDate,
                                               @RequestParam Optional<String> endDate,
                                               @RequestParam Optional<String> timeSpan){
        TimeSpanEnum time = timeSpan.isPresent() ? TimeSpanEnum.valueOf(timeSpan.get()) : TimeSpanEnum.THIS_WEEK;

        QueryDTO date = startDate.isPresent() && endDate.isPresent() && timeSpan.isEmpty() ? new QueryDTO(LocalDate.parse(startDate.get()), LocalDate.parse(endDate.get())) :
                answerService.getTimeSpan(time);
        System.out.println("Start date: " + date.getStartDate() + ", End date: " + date.getEndDate());
        return messageUtil.responseEntity(
                answerService.getUserAvgPeriods(time, userIds, date.getStartDate(), date.getEndDate()),
                "Here you go"
                , HttpStatus.OK);
    }

//    @RolesAllowed("user")
//    @GetMapping("/usertoken")
//    public String getHelloUser(@RequestHeader String Authorization, KeycloakAuthenticationToken jwt){
//        return "Hello user!";
//    }
//
//    @RolesAllowed("moderator")
//    @GetMapping("/moderatortoken")
//    public String getHelloModerator(@RequestHeader String Authorization, KeycloakAuthenticationToken jwt){
//
//        return "Hello moderator!";
//    }
//
//    @RolesAllowed({"user", "moderator"})
//    @GetMapping("/bothtoken")
//    public String getHelloBoth(@RequestHeader String Authorization, KeycloakAuthenticationToken jwt){
//        return "Hello both!";
//    }

//    @RolesAllowed({"user", "moderator"})
//    @GetMapping("/realmstoken")
//    public String getRealms(@RequestHeader String Authorization, KeycloakAuthenticationToken jwt){
////        System.out.println(keycloak.realm("Kundnojd-Realm").users().list().toString());
//        return "Hehe";
//    }
}
