package com.kundnojd.controller;

import com.kundnojd.DTO.LanguageQuestionDTO;
import com.kundnojd.DTO.QuestionsLanguagesDTO;
import com.kundnojd.enums.AnswerEnum;
import com.kundnojd.model.*;
import com.kundnojd.DTO.SurveyDTO;
import com.kundnojd.repository.IDTO.IQuestionDTO;
import com.kundnojd.repository.IDTO.ITranslatedQuestionDTO;
import com.kundnojd.repository.LanguageRepository;
import com.kundnojd.repository.AnswerRepository;
import com.kundnojd.services.*;
import com.kundnojd.util.MessageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/api")
public class SurveyController {

    @Autowired
    private SurveyService surveyService;
    @Autowired
    private AnswerService answerService;
    @Autowired
    private MessageUtil messageUtil;
    @Autowired
    private UserService userService;
    @Autowired
    private LanguageService languageService;
    @Autowired
    private QuestionService questionService;
    @Autowired
    private AnswerRepository answerRepository;
    @Autowired
    private LanguageRepository languageRepository;


    /**
     * TODO: fix or erase, currently "Zero date value prohibited"
     *
     */
//    @GetMapping("/surveys")
//    public ResponseEntity getAllSurveys() {
//        return messageUtil.responseEntity(surveyService.getAllSurveys(), "All surveys!", HttpStatus.OK);
//    }

    /**
     * The API aims to combine a technician_id with a list of questions, creating a "PENDING" survey that may be answered through /PUT /api/score in {@link com.kundnojd.controller.AnswerController} .
     * Simultaneously, {@link com.kundnojd.services.AnswerService} is being called to prefill the survey with scores of 0.
     * If any of the questions does not exist in the database, throw status.conflict!
     * Example request body below.
     * {
     *     "technician_id": 1,
     *     "questions": [
     *         {
     *             "id": 1
     *         },
     *         {
     *             "id": 2
     *         },
     *         {
     *             "id": 3
     *         }
     *     ]
     * }
     * @param surveyDTO A required object including {@link com.kundnojd.DTO.SurveyDTO}
     * @return A call to {@link com.kundnojd.services.AnswerService}, saving it to the DB.
     */
//    @PostMapping("/survey")
//    public ResponseEntity createSurvey(@RequestBody SurveyDTO surveyDTO) {
//        surveyDTO.setSurvey(surveyService.createSurvey(surveyDTO.getUserId()));
//
//        for (QuestionDTO q: surveyDTO.getQuestions()) {
//            if(questionService.getQuestionById(q.getId()).isEmpty()){
//                return messageUtil.responseEntity("No question on specified id", HttpStatus.CONFLICT);
//            }
//        }
//        List<Answer> answers = answerService.createScoresList(surveyDTO);
//        return answerService.saveToDB(answers, surveyDTO.getSurvey());
//    }

    @PostMapping("/survey")
    public ResponseEntity createSurveyAndLinkCustomer(@RequestBody SurveyDTO surveyDTO) {
        Optional<User> technician = userService.getUser(surveyDTO.getUserId());
        surveyDTO.setSurvey(surveyService.createSurveyAndLinkCustomer(surveyDTO.getUserId(),surveyDTO.getParticipantId(), surveyDTO.getSurvey().getId()));

        List<Question> questions = questionService.getActiveQuestions(technician.get().getRealm());

        List<Answer> answers = answerService.createScoresList(questions,surveyDTO.getSurvey());
        return answerService.saveToDB(answers, surveyDTO.getSurvey());
    }


    /**
     * The API will PUT a survey in the database, setting AnswerEnum to NO (1), as if a user does NOT want to participate in the survey.
     *
     * @param id Required Id of the survey
     * @return A call to {@link com.kundnojd.services.SurveyService}, saving it to the DB.
     */
    @PutMapping("/surveynoanswer/{id}")
    public ResponseEntity submitNoAnswer(@PathVariable("id") Integer id) {
        Optional<Survey> survey = surveyService.getSurveyById(id);
        if (survey.isEmpty()) {
            return messageUtil.responseEntity("Survey on id: " + id + "is not present", HttpStatus.NOT_FOUND);
        }
        Survey surveyDB = survey.get();
        if (!surveyService.isSurveyAnswer(surveyDB, AnswerEnum.PENDING)) {
            return messageUtil.responseEntity("Survey on id: " + id + " has already been evaluated", HttpStatus.NOT_FOUND);
        }
        surveyDB = surveyService.setEvaluatedAndDate(surveyDB, AnswerEnum.NO);
        return surveyService.saveToDB(surveyDB);
    }

    /**
     * The API will return the survey specified in the path variable, including the questions and languages associated to that specific survey.
     * The survey will be returned with the language specified in the param, if no param is included the language with lowest languageId available will be used.
     * The data from the response is the request when putting /api/score in {@link com.kundnojd.controller.AnswerController}
     *
     * @param surveyId Required Id of the survey
     * @param languageId Optional Id of the language
     * @return A response entity containing the {@link com.kundnojd.util.MessageUtil} which holds data and an explaining message through {@link com.kundnojd.model.CommonResponse}. Also the HttpStatus.
     */
    @GetMapping("/survey/{id}")
    public ResponseEntity getQuestions(@PathVariable("id") Integer surveyId, @RequestParam Optional<Integer> languageId) {

        Optional<Survey> survey = surveyService.getSurveyById(surveyId);
        if (survey.isEmpty()) {
            return messageUtil.responseEntity("Survey on id: " + surveyId + "is not present", HttpStatus.NOT_FOUND);
        }
        if(survey.get().getAnswer() != AnswerEnum.PENDING)
            return messageUtil.responseEntity("Survey has already been answered", HttpStatus.FORBIDDEN);
//        Survey surveyDB = survey.get();
//
//        List<QuestionDTO> questions = answerRepository.getAllQuestionBuSurveyIdAndLanguageId(surveyDB.getId(), languageId.get());
//        List<Language> languages = languageRepository.getAllLanguagesBySurveyId(surveyDB.getId());
//
//        SurveyDTO surveyDTO = new SurveyDTO(surveyDB, questions, languages);

//        return messageUtil.responseEntity(surveyDTO, "Survey found", HttpStatus.OK);
        //return messageUtil.responseEntity(answerRepository.getAllQuestionBySurveyIdAndLanguageId(surveyId,languageId.get()), "Survey found", HttpStatus.OK);
        Integer roleId = survey.get().getParticipant().getRole().getId();
        List<Language> languages = languageRepository.getAllLanguagesByRoleId(roleId);
        List<ITranslatedQuestionDTO> questions = answerRepository.getAllQuestionsByRoleIdAndLanguageId(roleId, languageId.get());
        return messageUtil.responseEntity(
                new QuestionsLanguagesDTO(questions, languages),
                "Survey found", HttpStatus.OK);
    }

    /**
     * The API will return the average score of the survey specified in the param.
     * The average will only be calculated if the survey has been answered correctly.
     *
     * @param id Required id of the survey
     * @return A response entity containing the {@link com.kundnojd.util.MessageUtil} which holds data and an explaining message through {@link com.kundnojd.model.CommonResponse}. Also the HttpStatus.
     *
     */
//    @GetMapping("/avgsurvey/{id}")
//    public ResponseEntity getAverageForSurvey(@PathVariable("id") Integer id) {
//        Optional<Survey> survey = surveyService.getSurveyById(id);
//
//        if (survey.isEmpty()) {
//            return messageUtil.responseEntity("Not present!", HttpStatus.NOT_FOUND);
//        }
//        if (!(surveyService.isSurveyAnswer(survey.get(), AnswerEnum.YES))) {
//            return messageUtil.responseEntity("Have not yet been answered!", HttpStatus.CONFLICT);
//        }
//
//        /*List<Score> scores = scoreService.getAllScoresBySurvey(survey.get());
//        if (scores.isEmpty()) {
//            return messageUtil.responseEntity("No scores found!", HttpStatus.NOT_FOUND);
//        }
//        if (scoreService.isNullInAnswer(scores)) {
//            return messageUtil.responseEntity("Some values had Null", HttpStatus.CONFLICT);
//        }*/
//        //scoreService.calculateAvg(scores)
//        return messageUtil.responseEntity(answerRepository.getAverageAnswerBySurveyId(id), "Average for the survey on id: " + id, HttpStatus.OK);
//    }
}
