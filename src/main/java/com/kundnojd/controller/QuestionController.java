package com.kundnojd.controller;

import com.kundnojd.model.*;
import com.kundnojd.DTO.*;
import com.kundnojd.repository.IDTO.IQuestionDTO;
import com.kundnojd.repository.IDTO.ITranslatedQuestionDTO;
import com.kundnojd.repository.LanguageRepository;
import com.kundnojd.repository.QuestionRepository;
import com.kundnojd.repository.AnswerRepository;
import com.kundnojd.services.*;
import com.kundnojd.util.DateUtil;
import com.kundnojd.util.MessageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class QuestionController {

    @Autowired
    private QuestionService questionService;
    @Autowired
    private MessageUtil messageUtil;
    @Autowired
    private UserService userService;
    @Autowired
    private SurveyService surveyService;
    @Autowired
    private ModeratorService moderatorService;
    @Autowired
    private LanguageQuestionService languageQuestionService;
    @Autowired
    private RealmService realmService;
    @Autowired
    private LanguageService languageService;
    @Autowired
    private AnswerService answerService;
    @Autowired
    private QuestionRepository questionRepository;
    @Autowired
    private AnswerRepository answerRepository;
    @Autowired
    private LanguageRepository languageRepository;
    @Autowired
    private DateUtil dateUtil;

//    @GetMapping("/activequestions/{id}")
//    public ResponseEntity getActiveQuestions(@PathVariable("id") Integer realmId, @RequestParam Optional<Integer> language_id){
//        Realm realm = realmService.getRealm(realmId).get();
//        List<Language> languages = languageRepository.getAllLanguagesByRealmIdAndActiveIs(realm.getId(), true);
//        Optional<Language> language = languageService.getLanguage(language_id, languages);
//        if (language.isEmpty()) {
//            return messageUtil.responseEntity("No language on ID" + language_id, HttpStatus.CONFLICT);
//        }
//        List<QuestionDTO> questionDTOS = questionRepository.getAllQuestionDTOByRealmIdAndLanguageIdAndQuestionIs(realm.getId(), language.get().getId(), true);
//
//        return messageUtil.responseEntity(new SurveyDTO(questionDTOS,languages), "All found", HttpStatus.OK);
//    }
    /**
     * TODO: in term of different languages
     * @return
     */

    @GetMapping("/questions/{id}")
    public ResponseEntity getQuestions(@PathVariable("id") Integer realmId, @RequestParam Optional<Integer> languageId) {

        Realm realm = realmService.getRealm(realmId).get();
        List<Language> languages = languageRepository.getAllLanguagesByRealmId(realm.getId());
        Optional<Language> language = languageService.getLanguage(languageId, languages);

        if (language.isEmpty()) {
            return messageUtil.responseEntity("No language on ID" + languageId, HttpStatus.CONFLICT);
        }
        List<ITranslatedQuestionDTO> questionDTOS = questionRepository.getALlTranslatedQuestionsByLanguageIdAndRealmId(language.get().getId(), realmId);
//        List<IQuestionDTO> notTranslatedQuestions = questionRepository.getNotTranslatedQuestionsByLanguageIdAndRealmId(language.get().getId(), realmId);

        List<Integer> questionIds = questionDTOS.stream().map(question -> question.getQuestionId()).collect(Collectors.toList());
        List<IQuestionDTO> notTranslatedQuestions =
                questionRepository.getNotTranslatedQuestionsByLanguageIdAndRealmIdAndNotInListId(languageId.get(), realmId, questionIds);
//        List<AllQuestionDTO> questionDTOS = questionRepository.getAllQuestionDTOsByLanguageIdAndRealmId(realm.getId(), language.get().getId());
//        List<AllQuestionDTO> notTranslatedQuestions = questionRepository.notTranslatedQuestions(realm.getId(),language.get().getId());
//
        return messageUtil.responseEntity(new AllQuestionDTOsLanguages(questionDTOS, languages,notTranslatedQuestions), "All found", HttpStatus.OK);
//        return messageUtil.responseEntity(realmId, "This has been modified", HttpStatus.OK);
    }

    /**
     * TODO språk
     * @param id
     * @return
     */
//    @GetMapping("/question/{id}")
//    public ResponseEntity getQuestionById(@PathVariable("id") Integer id) {
//        Optional<Question> question = questionService.getQuestionById(id);
//        if(question.isEmpty()){
//            return messageUtil.responseEntity("Not found", HttpStatus.CONFLICT);
//        }
//        return messageUtil.responseEntity(languageQuestionService.findLanguageQuestion(question.get()), "Found", HttpStatus.OK);
//    }

    /**
     * An API that aims to post a question as a moderator. The requestBody can include multiple translations to different languages.
     * The question is added to the realm of which the moderator exists.
     * Below is an example where a moderator on ID 1337 posts a question on two different languages simultaneously, languages on id 1 & 11..
     * {
     *     "moderatorID": 1337,
     *     "active": false,
     *     "languagesTextDTOList": [
     *         {
     *         "language": {
     *             "id": 1
     *         },
     *         "text": "Ett?"
     *         },
     *         {
     *         "language": {
     *             "id": 11
     *         },
     *         "text": "Ixi?"
     *         }
     *     ]
     * }
     * @param languageQuestionDTO Includes {@link com.kundnojd.DTO.LanguagesTextDTO} which holds a language object and a text string. It also includes moderator(int) and active(bool).
     * @return A response entity containing the {@link com.kundnojd.util.MessageUtil} which holds data and an explaining message through {@link com.kundnojd.model.CommonResponse}. Also the HttpStatus.
     */
    @PostMapping("/question")
    public ResponseEntity createQuestion(@RequestBody LanguageQuestionDTO languageQuestionDTO) {
        Optional<Moderator> moderator = moderatorService.findModeratorById(languageQuestionDTO.getModeratorID());
        if(moderator.isEmpty()){
            return messageUtil.responseEntity("Moderator not 1337 enough", HttpStatus.CONFLICT);
        }
        Question question = questionService.saveToDB(new Question(languageQuestionDTO.getActive(), moderator.get().getRealm()));

        return languageQuestionService.loopList(languageQuestionDTO.getLanguagesTextDTOList(), question);
    }

    /**
     * An API aiming to add a language with its language-text to an already existing questionId.
     *
     * {
     *     "questionID": 61,
     *     "languagesTextDTOList": [
     *         {
     *         "language": {
     *             "id": 21
     *         },
     *         "text": "Add question with languageID:21 at question 61.!"
     *         }
     *     ]
     * }
     *
     * @param languageQuestionDTO Includes questionID and languagesTextDTOList with a language object and a text string.
     * @return A response entity containing the {@link com.kundnojd.util.MessageUtil} which holds data and an explaining message through {@link com.kundnojd.model.CommonResponse}. Also the HttpStatus.
     */
    @PostMapping("/addLanguageToQuestion")
    public ResponseEntity addLanguageToQuestion(@RequestBody LanguageQuestionDTO languageQuestionDTO) {
        Optional<Question> question = questionService.getQuestionById(languageQuestionDTO.getQuestionID());
        if(question.isEmpty()){
            return messageUtil.responseEntity("Not found", HttpStatus.CONFLICT);
        }
        return languageQuestionService.loopList(languageQuestionDTO.getLanguagesTextDTOList(), question.get());
    }



//    @PutMapping("/question")
//    public ResponseEntity updateQuestion(@RequestBody Question question) {
//        Boolean isQuestion = questionService.existsById(question.getId());
//        if (isQuestion) {
//            Optional<Question> questionDB = questionService.getQuestionById(question.getId());
//            questionDB.get().setActive(question.getActive());
//            questionService.saveQuestion(questionDB.get());
//            return new ResponseEntity(HttpStatus.OK);
//        }
//        return new ResponseEntity(HttpStatus.NOT_FOUND);
//    }
//    @PutMapping("/question")
//    public ResponseEntity updateQuest(@RequestBody Question question){
//        Optional<Question> questionDB = questionService.getQuestionById(question.getId());
//        if(questionDB.isEmpty())
//            return messageUtil.responseEntity("No question on the given id", HttpStatus.NOT_FOUND);
//        return questionService.saveQuestion(questionService.setQuestionActive(questionDB.get()));
//    }

//    @DeleteMapping("/question/{id}")
//    public ResponseEntity deleteQuestion(@PathVariable("id") Integer id) {
//
//        return questionService.deleteQuestion(id);
//    }

    // TODO Erase request body in the getMApping.
    @GetMapping("/questions/user/{id}")
    public ResponseEntity getQuestionsByUser(@PathVariable("id") Integer userId,
                                                   @RequestParam Optional<String> startDate,
                                                   @RequestParam Optional<String> endDate,
                                                   @RequestParam Optional<String> timeSpan,
                                                   @RequestParam Optional<Integer> languageId,
                                                   @RequestParam Integer page,
                                                   @RequestParam Integer pageAmount){

        QueryDTO date = dateUtil.getStartAndEndDate(startDate,endDate,timeSpan);

        try{
            return messageUtil.pageResponseEntityOK(
                    answerRepository.getAllModQuestionsByLanguageIdAndDatesBetweenAndUserId(
                            languageId.get(),
                            date.getStartDate(),
                            date.getEndDate(),
                            userId, PageRequest.of(page, pageAmount)), "Succeeded");
        } catch (Exception ex){
            System.out.println(ex.getMessage());
            return messageUtil.responseEntity("Server error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
