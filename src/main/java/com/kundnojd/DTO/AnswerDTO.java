package com.kundnojd.DTO;


public class AnswerDTO {

    private Integer id;

    private Integer survey_id;

    private Integer question_id;

    private Integer value;

    public AnswerDTO(Integer survey_id, Integer question_id, Integer value) {
        this.survey_id = survey_id;
        this.question_id = question_id;
        this.value = value;
    }

    public AnswerDTO(Integer survey_id, Integer question_id) {
        this.survey_id = survey_id;
        this.question_id = question_id;
    }

    public AnswerDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSurvey_id() {
        return survey_id;
    }

    public void setSurvey_id(Integer survey_id) {
        this.survey_id = survey_id;
    }

    public Integer getQuestion_id() {
        return question_id;
    }

    public void setQuestion_id(Integer question_id) {
        this.question_id = question_id;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
