package com.kundnojd.DTO;

import java.util.List;

public class LanguageQuestionDTO {
    private List<LanguagesTextDTO> languagesTextDTOList;
    private Integer moderatorID;
    private Boolean active;
    private Integer questionID;

    public LanguageQuestionDTO(List<LanguagesTextDTO> languagesTextDTOList, Integer moderatorID, Boolean active) {
        this.languagesTextDTOList = languagesTextDTOList;
        this.moderatorID = moderatorID;
        this.active = active;
    }

    public LanguageQuestionDTO(List<LanguagesTextDTO> languagesTextDTOList, Integer moderatorID, Integer questionID) {
        this.languagesTextDTOList = languagesTextDTOList;
        this.questionID = questionID;
    }

    public LanguageQuestionDTO() {
    }

    public List<LanguagesTextDTO> getLanguagesTextDTOList() {
        return languagesTextDTOList;
    }

    public void setLanguagesTextDTOList(List<LanguagesTextDTO> languagesTextDTOList) {
        this.languagesTextDTOList = languagesTextDTOList;
    }

    public Integer getQuestionID() {
        return questionID;
    }

    public void setQuestionID(Integer questionID) {
        this.questionID = questionID;
    }

    public Integer getModeratorID() {
        return moderatorID;
    }

    public void setModeratorID(Integer moderatorID) {
        this.moderatorID = moderatorID;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}


