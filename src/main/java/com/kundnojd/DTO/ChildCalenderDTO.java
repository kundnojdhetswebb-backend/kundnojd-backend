package com.kundnojd.DTO;

import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.kundnojd.enums.CalendarTypeEnum;
import com.kundnojd.enums.WeekdaysEnum;

import java.time.LocalDate;


public class ChildCalenderDTO {
    private Integer answeredYes = 0;
    private Integer answeredNo = 0;
    private Integer number;
    private Double avgScore;

    public ChildCalenderDTO() {
    }

    public ChildCalenderDTO(LocalDate localDate, long answeredYes, long answeredNo, int enumValue, Double avgScore) {
        this.answeredYes = (int) answeredYes;
        this.answeredNo = (int) answeredNo;

//        this.number = switch (enumValue) {
//            case 1,2 -> localDate.getDayOfWeek().getValue();
//            case 3 -> localDate.getDayOfMonth();
//            case 4 -> localDate.getDayOfYear();
//            default -> 0;
//        };
        this.number = 1;
        this.avgScore = avgScore;
    }
    public ChildCalenderDTO(int number, long answeredYes, long answeredNo, Double avgScore) {
        this.answeredYes = (int) answeredYes;
        this.answeredNo = (int) answeredNo;

        this.number = number;
        this.avgScore = avgScore;
    }
    public ChildCalenderDTO(int number, long answeredYes, long answeredNo, Double avgScore, long countSurveyId) {

        long numberOfDuplicates = (answeredYes + answeredNo) / countSurveyId;

        this.answeredYes = (int) answeredYes / (int) numberOfDuplicates;
        this.answeredNo = (int) answeredNo / (int) numberOfDuplicates;
        this.number = number;
        this.avgScore = avgScore;
    }


    /**
     * DTO that aims to present daily statistics. In terms of what day it is(ENUM) and what day it is.
     * @param answeredYes
     * @param answeredNo
     */


    public ChildCalenderDTO(Integer number, Integer answeredYes, Integer answeredNo) {
        this.number = number;
        this.answeredYes = answeredYes;
        this.answeredNo = answeredNo;
    }
    public ChildCalenderDTO(LocalDate localDate, long answeredYes, int answeredNo) {
        this.number = localDate.getDayOfMonth();
        this.answeredYes = (int) answeredYes;
        this.answeredNo = (int) answeredNo;
        System.out.println("CTOR 1");
    }

    public ChildCalenderDTO(LocalDate localDate, long answeredYes, long answeredNo, int enumValue) {
        this.answeredYes = (int) answeredYes;
        this.answeredNo = (int) answeredNo;

//        this.number = switch (enumValue) {
//            case 1,2 -> localDate.getDayOfWeek().getValue();
//            case 3 -> localDate.getDayOfMonth();
//            case 4 -> localDate.getDayOfYear();
//            default -> 0;
//        };
        this.number = 1;
    }

    public Integer getAnsweredYes() {
        return answeredYes;
    }

    public void setAnsweredYes(Integer answeredYes) {
        this.answeredYes = answeredYes;
    }

    public Integer getAnsweredNo() {
        return answeredNo;
    }

    public void setAnsweredNo(Integer answeredNo) {
        this.answeredNo = answeredNo;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Double getAvgScore() {
        return avgScore;
    }

    public void setAvgScore(Double avgScore) {
        this.avgScore = avgScore;
    }
}
