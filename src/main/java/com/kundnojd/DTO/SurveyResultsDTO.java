package com.kundnojd.DTO;

import com.kundnojd.enums.AnswerEnum;
import com.kundnojd.model.Answer;
import com.kundnojd.model.Survey;

import java.time.LocalDate;
import java.util.List;

public class SurveyResultsDTO {
    private double average;
    private Integer total;
    private Boolean isAnswered;
    private LocalDate date;
    private Integer userId;

    public SurveyResultsDTO() {
    }

    public SurveyResultsDTO(Survey survey, List<Answer> answer) {
        this.total = answer.stream().mapToInt(i -> i.getValue()).sum();
        this.average = this.total / answer.size();
        this.isAnswered = survey.getAnswer() == AnswerEnum.YES;
        this.date = survey.getDate();
        this.userId = survey.getUser().getId();
    }

    public double getAverage() {
        return average;
    }

    public Integer getTotal() {
        return total;
    }

    public Boolean getAnswered() {
        return isAnswered;
    }

    public LocalDate getDate() {
        return date;
    }

    public Integer getUserId() {
        return userId;
    }
}
