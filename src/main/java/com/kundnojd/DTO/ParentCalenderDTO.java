package com.kundnojd.DTO;

import com.kundnojd.enums.CalendarTypeEnum;

import java.util.List;

public class ParentCalenderDTO {

    private double totalScore = 0;
    private double avgScore = 0;
    private double avgResponseRate = 0;
    private List<ChildCalenderDTO> children;
    private CalendarTypeEnum calendarType;

    public ParentCalenderDTO() {
    }

    public ParentCalenderDTO(double avgResponseRate) {
        this.avgResponseRate = avgResponseRate;
    }

    public ParentCalenderDTO(double avgScore, long totalScore ) {
        this.avgScore = avgScore;
        this.totalScore = (double) totalScore;
    }

    public ParentCalenderDTO(double totalScore, double avgScore, double avgResponseRate, List<ChildCalenderDTO> children, CalendarTypeEnum calendarType) {
        this.totalScore = totalScore;
        this.avgScore = avgScore;
        this.avgResponseRate = avgResponseRate;
        this.children = children;
        this.calendarType = calendarType;
    }

    public double getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(double totalScore) {
        this.totalScore = totalScore;
    }

    public double getAvgScore() {
        return avgScore;
    }

    public void setAvgScore(double avgScore) {
        this.avgScore = avgScore;
    }

    public double getAvgResponseRate() {
        return avgResponseRate;
    }

    public void setAvgResponseRate(double avgResponseRate) {
        this.avgResponseRate = avgResponseRate;
    }

    public List<ChildCalenderDTO> getChildren() {
        return children;
    }

    public void setChildren(List<ChildCalenderDTO> children) {
        this.children = children;
    }

    public CalendarTypeEnum getCalendarType() {
        return calendarType;
    }

    public void setCalendarType(CalendarTypeEnum calendarType) {
        this.calendarType = calendarType;
    }
}
