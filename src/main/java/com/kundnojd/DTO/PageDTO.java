package com.kundnojd.DTO;

public class PageDTO {
    private Object object;
    private Boolean hasPrevious;
    private Boolean hasNext;

    public PageDTO() {
    }

    public PageDTO(Object object, Boolean hasPrevious, Boolean hasNext) {
        this.object = object;
        this.hasPrevious = hasPrevious;
        this.hasNext = hasNext;
    }

    public Object getObject() {
        return object;
    }

    public Boolean getHasPrevious() {
        return hasPrevious;
    }

    public Boolean getHasNext() {
        return hasNext;
    }
}
