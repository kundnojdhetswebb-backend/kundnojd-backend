package com.kundnojd.DTO;

public class QuestionDTO {
    private Integer questionId;
    private String text;
    private Integer answerValue;
    private String answerText;
    private Integer answerId;

    public QuestionDTO() {
    }

    public QuestionDTO(Integer questionId, String text) {
        this.questionId = questionId;
        this.text = text;
    }

    public QuestionDTO(Integer questionId, String text, Integer answerValue) {
        this.questionId = questionId;
        this.text = text;
        this.answerValue = answerValue;
    }

    public QuestionDTO(Integer questionId, Integer answerValue, Integer answerId) {
        this.questionId = questionId;
        this.answerValue = answerValue;
        this.answerId = answerId;
    }

    public QuestionDTO(Integer questionId, Integer answerValue, String text) {
        this.questionId = questionId;
        this.answerValue = answerValue;
        this.text = text;
    }

    public QuestionDTO(Integer questionId, String text, Integer answerValue, String answerText, Integer answerId) {
        this.questionId = questionId;
        this.text = text;
        this.answerValue = answerValue;
        this.answerText = answerText;
        this.answerId = answerId;
    }

    public QuestionDTO(Integer questionId, String text, Integer answerValue, Integer answerId) {
        this.questionId = questionId;
        this.text = text;
        this.answerValue = answerValue;
        this.answerId = answerId;
    }


    public QuestionDTO(Integer questionId) {
        this.questionId = questionId;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Integer questionId) {
        this.questionId = questionId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getAnswerValue() {
        return answerValue;
    }

    public void setAnswerValue(Integer answerValue) {
        this.answerValue = answerValue;
    }

    public String getAnswerText() {
        return answerText;
    }

    public void setAnswerText(String answerText) {
        this.answerText = answerText;
    }

    public Integer getAnswerId() {
        return answerId;
    }

    public void setAnswerId(Integer answerId) {
        this.answerId = answerId;
    }
}
