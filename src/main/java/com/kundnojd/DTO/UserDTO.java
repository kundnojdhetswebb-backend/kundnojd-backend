package com.kundnojd.DTO;

import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.SqlResultSetMapping;

/*@SqlResultSetMapping(name = "TechnicianDTO", classes = {@ConstructorResult(targetClass = TechnicianDTO.class,
columns = {
        @ColumnResult(name = "id", type = Integer.class),
        //@ColumnResult(name = "answeredSurveys", type = Long.class)

        })})*/
public class UserDTO {
    private Integer id;
    private String name;
    private Integer answeredSurveys;
    private Integer answeredYesSurveys;
    private Double averageScore;
    private Integer totalScore;

    public UserDTO() {
    }

    public UserDTO(Integer id, long answeredSurveys, Double averageScore, long totalScore) {
        this.id = id;
        this.answeredSurveys = (int) answeredSurveys;
        this.averageScore = averageScore;
        this.totalScore = (int) totalScore;
    }

    public UserDTO(int id, long answeredSurveys, long answeredYesSurveys, double averageScore, long totalScore) {
        this.id = id;
        this.answeredSurveys = (int) answeredSurveys;
        this.answeredYesSurveys = (int) answeredYesSurveys;
        this.averageScore = averageScore;
        this.totalScore = (int) totalScore;
    }

    public UserDTO(Integer id, String name, Integer answeredSurveys, Integer answeredYesSurveys, Double averageScore, Integer totalScore) {
        this.id = id;
        this.name = name;
        this.answeredSurveys = answeredSurveys;
        this.answeredYesSurveys = answeredYesSurveys;
        this.averageScore = averageScore;
        this.totalScore = totalScore;
    }


    public Integer getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(Integer totalScore) {
        this.totalScore = totalScore;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAnsweredSurveys() {
        return answeredSurveys;
    }

    public void setAnsweredSurveys(Integer answeredSurveys) {
        this.answeredSurveys = answeredSurveys;
    }

    public Integer getAnsweredYesSurveys() {
        return answeredYesSurveys;
    }

    public void setAnsweredYesSurveys(Integer answeredYesSurveys) {
        this.answeredYesSurveys = answeredYesSurveys;
    }

    public Double getAverageScore() {
        return averageScore;
    }

    public void setAverageScore(Double averageScore) {
        this.averageScore = averageScore;
    }
}
