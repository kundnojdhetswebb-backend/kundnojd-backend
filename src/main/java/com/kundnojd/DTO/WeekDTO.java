package com.kundnojd.DTO;

import java.util.List;

public class WeekDTO {

    private double totalScore = 0;
    private double avgScore = 0;
    private double avgResponseRate = 0;
    private List<ChildCalenderDTO> daysOfWeeks;


    public WeekDTO() {
    }

    public WeekDTO(double avgScore, double avgResponseRate, List<ChildCalenderDTO> daysOfWeeks, double totalScore) {
        this.avgScore = avgScore;
        this.avgResponseRate = avgResponseRate;
        this.daysOfWeeks = daysOfWeeks;
        this.totalScore = totalScore;
    }


    public double getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(double totalScore) {
        this.totalScore = totalScore;
    }

    public double getAvgScore() {
        return avgScore;
    }

    public void setAvgScore(double avgScore) {
        this.avgScore = avgScore;
    }

    public double getAvgResponseRate() {
        return avgResponseRate;
    }

    public void setAvgResponseRate(double avgResponseRate) {
        this.avgResponseRate = avgResponseRate;
    }

    public List<ChildCalenderDTO> getDaysOfWeeks() {
        return daysOfWeeks;
    }

    public void setDaysOfWeeks(List<ChildCalenderDTO> daysOfWeeks) {
        this.daysOfWeeks = daysOfWeeks;
    }

}
