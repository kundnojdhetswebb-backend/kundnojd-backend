package com.kundnojd.DTO;

import com.kundnojd.model.Language;
import com.kundnojd.repository.IDTO.IQuestionDTO;
import com.kundnojd.repository.IDTO.ITranslatedQuestionDTO;

import java.util.List;

public class QuestionsLanguagesDTO {
    private List<ITranslatedQuestionDTO> questions;
    private List<Language> languages;

    public QuestionsLanguagesDTO(){ }
    public QuestionsLanguagesDTO(List<ITranslatedQuestionDTO> questions, List<Language> languages){
        this.questions = questions;
        this.languages = languages;
    }

    public List<ITranslatedQuestionDTO> getQuestions() {
        return questions;
    }

    public List<Language> getLanguages() {
        return languages;
    }
}
