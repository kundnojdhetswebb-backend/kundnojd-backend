package com.kundnojd.DTO;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class ModQuestionDTO {
    private Integer id;
    private String text;
    private Integer occurancesSurvey = 0;
    private Double averageScore;

    @JsonIgnore
    private Integer totalScore = 0;

    public ModQuestionDTO() {
    }

    public ModQuestionDTO(long occurancesSurvey, double averageScore) {
        this.occurancesSurvey = (int) occurancesSurvey;
        this.averageScore = averageScore;
    }

    public ModQuestionDTO(Integer id, String text) {
        this.id = id;
        this.text = text;
    }

    public ModQuestionDTO(Integer id, String text, Integer score) {
        this.id = id;
        this.text = text;
        this.occurancesSurvey++;
        this.totalScore += score;
    }

    public ModQuestionDTO(Integer id, String text, long occurancesSurvey, Double averageScore) {
        this.id = id;
        this.text = text;
        this.occurancesSurvey = (int) occurancesSurvey;
        this.averageScore = averageScore;
    }
    public ModQuestionDTO(Integer id, String text, long occurancesSurvey, Double averageScore, Boolean isActive) {
        this.id = id;
        this.text = text;
        this.occurancesSurvey = (int) occurancesSurvey;
        this.averageScore = averageScore;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getOccurancesSurvey() {
        return occurancesSurvey;
    }

    public void setOccurancesSurvey(Integer occurancesSurvey) {
        this.occurancesSurvey = occurancesSurvey;
    }

    public Double getAverageScore() {
        //return new BigDecimal(this.totalScore / (double) this.occurancesSurvey).setScale(2, RoundingMode.HALF_UP).doubleValue();
        return this.averageScore;
    }

    public void setAverageScore(Double averageScore) {
        this.averageScore = averageScore;
    }

    public Integer getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(Integer totalScore) {
        this.totalScore = totalScore;
    }
}
