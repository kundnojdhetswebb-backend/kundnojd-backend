package com.kundnojd.DTO;

import com.kundnojd.model.Language;
import com.kundnojd.repository.IDTO.IQuestionDTO;
import com.kundnojd.repository.IDTO.ITranslatedQuestionDTO;

import java.util.List;

public class AllQuestionDTOsLanguages {
    List<ITranslatedQuestionDTO> questions;
    List<IQuestionDTO> notTranslatedQuestions;
    List<Language> languages;

    public AllQuestionDTOsLanguages(){}
    public AllQuestionDTOsLanguages(List<ITranslatedQuestionDTO> questions, List<Language> languages, List<IQuestionDTO> notTranslatedQuestions) {
        this.questions = questions;
        this.languages = languages;
        this.notTranslatedQuestions = notTranslatedQuestions;
    }

    public List<ITranslatedQuestionDTO> getQuestions() {
        return questions;
    }

    public void setQuestions(List<ITranslatedQuestionDTO> questions) {
        this.questions = questions;
    }

    public List<Language> getLanguages() {
        return languages;
    }

    public void setLanguages(List<Language> languages) {
        this.languages = languages;
    }

    public List<IQuestionDTO> getNotTranslatedQuestions() {
        return notTranslatedQuestions;
    }

    public void setNotTranslatedQuestions(List<IQuestionDTO> notTranslatedQuestions) {
        this.notTranslatedQuestions = notTranslatedQuestions;
    }
}
