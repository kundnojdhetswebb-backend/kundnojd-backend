package com.kundnojd.DTO;

public class AllQuestionDTO {
    private Integer id;
    private Boolean isActive;
    private String text;

    public AllQuestionDTO(){}

    public AllQuestionDTO(Integer id, Boolean isActive) {
        this.id = id;
        this.isActive = isActive;
    }

    public AllQuestionDTO(Integer id, Boolean isActive, String text) {
        this.id = id;
        this.isActive = isActive;
        this.text = text;
    }

    public Integer getId() {
        return id;
    }

    public Boolean getActive() {
        return isActive;
    }

    public String getText() {
        return text;
    }
}
