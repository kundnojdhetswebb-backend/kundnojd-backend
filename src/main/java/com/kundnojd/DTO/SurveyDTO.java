package com.kundnojd.DTO;

import com.kundnojd.model.Language;
import com.kundnojd.model.Survey;

import java.util.*;

public class SurveyDTO {
    private Survey survey;
    private List<QuestionDTO> questions;
    private Integer userId;
    private Integer participantId;
    private List<Language> languages;

    public SurveyDTO() {
    }

    public SurveyDTO(Survey survey, List<QuestionDTO> questions) {
        this.survey = survey;
        this.questions = questions;
    }
    public SurveyDTO(List<QuestionDTO> questions, Integer userId) {
        this.userId = userId;
        this.questions = questions;
    }

    public SurveyDTO(List<QuestionDTO> questions, List<Language> languages) {
        this.questions = questions;
        this.languages = languages;
    }


    public SurveyDTO(Survey survey, List<QuestionDTO> questions, List<Language> languages) {
        this.survey = survey;
        this.questions = questions;
        this.languages = languages;
    }

    public List<Language> getLanguages() {
        return languages;
    }

    public void setLanguages(List<Language> languages) {
        this.languages = languages;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Survey getSurvey() {
        return survey;
    }

    public void setSurvey(Survey survey) {
        this.survey = survey;
    }

    public List<QuestionDTO> getQuestions() {
        return questions;
    }

    public void setQuestions(List<QuestionDTO> questions) {
        this.questions = questions;
    }

    public Integer getParticipantId() {
        return participantId;
    }

    public void setParticipantId(Integer participantId) {
        this.participantId = participantId;
    }
}
