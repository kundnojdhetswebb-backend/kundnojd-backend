package com.kundnojd.DTO;

import java.util.List;

public class MonthDTO {
    private double totalScore = 0;
    private double avgScore = 0;
    private double avgResponseRate = 0;
    private List<ChildCalenderDTO> weeksOfMonth;

    public MonthDTO() {
    }

    public MonthDTO(double totalScore, double avgScore, double avgResponseRate, List<ChildCalenderDTO> weeksOfMonth) {
        this.totalScore = totalScore;
        this.avgScore = avgScore;
        this.avgResponseRate = avgResponseRate;
        this.weeksOfMonth = weeksOfMonth;
    }

    public double getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(double totalScore) {
        this.totalScore = totalScore;
    }

    public double getAvgScore() {
        return avgScore;
    }

    public void setAvgScore(double avgScore) {
        this.avgScore = avgScore;
    }

    public double getAvgResponseRate() {
        return avgResponseRate;
    }

    public void setAvgResponseRate(double avgResponseRate) {
        this.avgResponseRate = avgResponseRate;
    }

    public List<ChildCalenderDTO> getWeeksOfMonth() {
        return weeksOfMonth;
    }

    public void setWeeksOfMonth(List<ChildCalenderDTO> weeksOfMonth) {
        this.weeksOfMonth = weeksOfMonth;
    }

}
