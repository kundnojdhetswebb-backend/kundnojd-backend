package com.kundnojd.DTO;

import com.kundnojd.model.Language;

public class LanguagesTextDTO {
    private Language language;
    private String text;

    public LanguagesTextDTO(Language language, String text) {
        this.language = language;
        this.text = text;
    }

    public LanguagesTextDTO() {
    }

    public Language getLanguage() {
        return language;
    }

    public String getText() {
        return text;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public void setText(String text) {
        this.text = text;
    }
}
