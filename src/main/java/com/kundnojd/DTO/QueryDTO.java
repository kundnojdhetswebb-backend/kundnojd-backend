package com.kundnojd.DTO;
import com.kundnojd.enums.TimeSpanEnum;

import java.time.LocalDate;
import java.util.*;

public class QueryDTO {
    private TimeSpanEnum timeSpan;
    private LocalDate startDate;
    private LocalDate endDate;
    private List<Integer> userIds;
    private Integer languageId;
    private Integer realmId;
    private List<Integer> questionIds;

    public QueryDTO() {
    }


    public QueryDTO(TimeSpanEnum timeSpan) {
        this.timeSpan = timeSpan;
    }

    public QueryDTO(Integer languageId) {
        this.languageId = languageId;
    }

    public QueryDTO(LocalDate startDate, LocalDate endDate, List<Integer> userIds, Integer languageId, Integer realmId, List<Integer> questionIds, TimeSpanEnum timeSpan) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.userIds = userIds;
        this.languageId = languageId;
        this.realmId = realmId;
        this.questionIds = questionIds;
        this.timeSpan = timeSpan;
    }

    public QueryDTO(LocalDate startDate) {
        this.startDate = startDate;
    }

    public QueryDTO(LocalDate startDate, LocalDate endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public List<Integer> getUserIds() {
        return userIds;
    }

    public void setUserIds(List<Integer> userIds) {
        this.userIds = userIds;
    }

    public Integer getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Integer languageId) {
        this.languageId = languageId;
    }

    public Integer getRealmId() {
        return realmId;
    }

    public void setRealmId(Integer realmId) {
        this.realmId = realmId;
    }

    public List<Integer> getQuestionIds() {
        return questionIds;
    }

    public void setQuestionIds(List<Integer> questionIds) {
        this.questionIds = questionIds;
    }

    public TimeSpanEnum getTimeSpan() {
        return timeSpan;
    }

    public void setTimeSpan(TimeSpanEnum timeSpan) {
        this.timeSpan = timeSpan;
    }
}
