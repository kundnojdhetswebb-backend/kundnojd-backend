package com.kundnojd.repository;

import com.kundnojd.model.Language;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository

public interface LanguageRepository extends JpaRepository<Language, Integer> {
    Language findByName(String name);

    @Query(value = "SELECT * FROM language WHERE language.id IN \n" +
            "(SELECT language_question.language_id FROM language_question \n" +
            "WHERE language_question.question_id IN(SELECT answer.question_id FROM answer WHERE answer.survey_id = ?1))", nativeQuery = true)
    List<Language> getAllLanguagesBySurveyId(Integer surveyId);

    @Query( value =  "SELECT * FROM statsdb.language language WHERE language.id IN \n" +
            "(SELECT language_question.language_id FROM statsdb.language_question language_question \n" +
            "WHERE language_question.question_id IN (SELECT question.id FROM statsdb.question question " +
            "WHERE question.realm_id = ?1 AND question.is_active = ?2))", nativeQuery = true)
    List<Language> getAllLanguagesByRealmIdAndActiveIs(Integer realmId, boolean isActive);

    @Query( value =  "SELECT * FROM statsdb.language language WHERE language.id IN \n" +
            "(SELECT language_question.language_id FROM statsdb.language_question language_question \n" +
            "WHERE language_question.question_id IN (SELECT question.id FROM statsdb.question question " +
            "WHERE question.realm_id = ?1))", nativeQuery = true)
    List<Language> getAllLanguagesByRealmId(Integer realmId);

    @Query ( value = "SELECT l.id, l.name FROM language l\n" +
            "INNER JOIN language_question l_q ON l.id = l_q.language_id\n" +
            "INNER JOIN question_role q_r ON l_q.question_id = q_r.question_id\n" +
            "WHERE q_r.role_id = 2\n" +
            "GROUP BY l.id, l.name", nativeQuery = true)
    List<Language> getAllLanguagesByRoleId(Integer roleId);
}
