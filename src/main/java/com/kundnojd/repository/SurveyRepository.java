package com.kundnojd.repository;

import com.kundnojd.enums.AnswerEnum;
import com.kundnojd.model.Survey;
import com.kundnojd.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.*;

@Repository
public interface SurveyRepository extends JpaRepository<Survey, Integer> {
//    List<Survey> findAllByTechnician(User user);
//    List<Survey> findAllByTechnicianAndAndAnswer(User user, AnswerEnum answer);
//    List<Survey> findAllByTechnicianAndDateIsAfter(User user, LocalDate date);
//    List<Survey> findAllByTechnicianAndDateGreaterThanEqualAndDateLessThanEqual(User user, LocalDate startDate, LocalDate endDate);
//    List<Survey> findAllByTechnicianAndDateIs(User user, LocalDate startDate);
//    List<Survey> findAllByTechnicianAndDateGreaterThanEqualAndDateLessThanEqualAndAnswer(User user, LocalDate startDate, LocalDate endDate, AnswerEnum answerEnum);

    @Query(value = "SELECT COUNT(id) FROM survey WHERE date >= ?1 AND date <= ?2 AND user_id = ?3 AND answer = ?4", nativeQuery = true)
    Integer getUserCountAnsweredSurveyBetweenDates(LocalDate startDate, LocalDate endDate, Integer userId, Integer answerEnum);

    @Query(value = "SELECT COUNT(id) FROM survey WHERE date >= ?1 AND date <= ?2 AND user_id = ?3", nativeQuery = true)
    Integer getUserCountTotalSurveyBetweenDates(LocalDate startDate, LocalDate endDate, Integer userId);

    @Query(value = "SELECT (SUM(CASE WHEN (survey.answer = 2) THEN 1 ELSE 0 END)/COUNT(survey.id) * 100) FROM survey \n" +
            "WHERE survey.date >= ?1 AND survey.date <= ?2 AND survey.user_id = ?3", nativeQuery = true)
    Double getResponseRatePercentByTechnicianIdAndBetweenDates(LocalDate startDate, LocalDate endDate, Integer userId);

//    Optional<Survey> getSurveyByIdAndUserId(Integer id, Integer userId);

}
