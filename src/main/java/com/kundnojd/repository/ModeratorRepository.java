package com.kundnojd.repository;

import com.kundnojd.model.Moderator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository

public interface ModeratorRepository extends JpaRepository<Moderator, Integer> {
}
