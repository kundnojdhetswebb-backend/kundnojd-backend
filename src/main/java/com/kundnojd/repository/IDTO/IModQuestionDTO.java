package com.kundnojd.repository.IDTO;

public interface IModQuestionDTO {
    Integer getQuestionId();
    String getText();
    Integer getSurveyAmount();
    Double getAnswerValue();
    String getAnswerText();
}
