package com.kundnojd.repository.IDTO;

public interface IUserAvgPeriod {
    Integer getUserId();
    Double getAverage();
    Integer getNumber();
}
