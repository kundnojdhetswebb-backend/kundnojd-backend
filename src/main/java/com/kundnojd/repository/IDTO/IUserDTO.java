package com.kundnojd.repository.IDTO;

public interface IUserDTO {
    Integer getUserId();
    Integer getSurveyId();
    Double getAnswerValue();
    Double getAnswerSum();
}
