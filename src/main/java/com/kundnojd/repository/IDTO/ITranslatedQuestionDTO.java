package com.kundnojd.repository.IDTO;

public interface ITranslatedQuestionDTO extends IQuestionDTO{
    String getText();
}
