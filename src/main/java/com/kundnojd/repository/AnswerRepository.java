package com.kundnojd.repository;

import com.kundnojd.DTO.*;
import com.kundnojd.model.Question;
import com.kundnojd.model.Answer;
import com.kundnojd.model.Survey;
import com.kundnojd.repository.IDTO.*;
import org.apache.tomcat.jni.Local;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.*;
@Repository
public interface AnswerRepository extends JpaRepository<Answer,Integer> {
    List<Answer> findAllBySurvey(Survey survey);
    Optional <Answer> findFirstBySurveyAndQuestion(Survey survey, Question question);

//    //@Query(value = "SELECT * FROM SCORE WHERE ID IN (SELECT ID FROM CASE WHERE answered <= ?1 AND answered >= ?2)", nativeQuery = true)
//    @Query(value = "SELECT * FROM ANSWER WHERE survey.Id = ?1", nativeQuery = true)
//    List<Answer> findAllBySurveyId(Integer surveyId);
//
//    @Query(value = "SELECT * FROM answer WHERE survey_id IN (SELECT id FROM survey WHERE date >= ?1 AND date <= ?2 AND user_id = ?3)", nativeQuery = true)
//    List<Answer> findAllBySurveyBetweenDatesAndTechnician(LocalDate startDate, LocalDate endDate, Integer technicianId);

    @Query(value = " SELECT AVG(value) FROM answer WHERE survey_id IN (SELECT id FROM survey WHERE date >= ?1 AND date <= ?2 AND user_id = ?3)", nativeQuery = true)
    Double getUserAnswerAverageBetweenDates(LocalDate startDate, LocalDate endDate, Integer userId);

    @Query(value = "SELECT AVG(value) FROM answer WHERE survey_id IN (SELECT id FROM survey WHERE user_id IN (SELECT id FROM user WHERE realm_id = ?1))", nativeQuery = true)
    Optional<Double> getAverageAnswerByRealmId(Integer realmId);
    //SELECT new com.kunnojd.DTO.ModQuestionDTO(l.question_id, l.text) FROM language_question l
    @Query(value = "SELECT new com.kundnojd.DTO.ModQuestionDTO(COUNT(a.id), AVG(a.value)) FROM Answer a WHERE a.question.id = ?1 AND a.survey.id IN " +
            "(SELECT s.id FROM Survey s WHERE s.date >= ?2 AND s.date <= ?3 AND s.user.id = ?4)")
    ModQuestionDTO getAllModQuestionsByQuestionIdAndDatesBetweenAndUserId(Integer questionId, LocalDate startDate, LocalDate endDate, Integer userId);

    @Query(value = "SELECT new com.kundnojd.DTO.ModQuestionDTO(COUNT(a.id), AVG(a.value)) FROM Answer a WHERE a.question.id = ?1 AND a.survey.id IN " +
            "(SELECT s.id FROM Survey s WHERE s.date >= ?2 AND s.date <= ?3 AND s.user.id IN (SELECT u.id FROM User u WHERE u.realm.id = ?4))")
    ModQuestionDTO getAllModQuestionsByQuestionIdAndAndDatesBetweenAndRealmId(Integer questionId, LocalDate startDate, LocalDate endDate, Integer realmId);

    @Query(value = "SELECT new com.kundnojd.DTO.UserDTO(user.id, COUNT(DISTINCT survey.id), AVG(answer.value), SUM(answer.value)) FROM Answer answer " +
            "INNER JOIN Survey survey ON answer.survey.id = survey.id " +
            "INNER JOIN User user ON survey.user.id = user.id WHERE user.realm.id = ?1 AND survey.date >= ?2 AND survey.date <= ?3 GROUP BY user.id")
    List<UserDTO> getModQuestionsByRealmIdAndBetweenDates(Integer realmId, LocalDate startDate, LocalDate endDate);

//    @Query(value = "SELECT new com.kundnojd.DTO.UserDTO(user.id, COUNT(DISTINCT survey.id), AVG(answer.value), SUM(answer.value)) FROM Answer answer " +
//            "INNER JOIN Survey survey ON answer.survey.id = survey.id " +
//            "INNER JOIN User user ON survey.user.id = user.id WHERE user.realm.id = ?1 AND survey.date >= ?2 AND survey.date <= ?3 GROUP BY user.id")
//    Slice<UserDTO> getModQuestionsByRealmIdAndBetweenDatesPage(Integer realmId, LocalDate startDate, LocalDate endDate, Pageable pageable);
    @Query(value = "SELECT user.id as userId, COUNT(DISTINCT survey.id) as surveyId, AVG(answer.value)  as answerValue, SUM(answer.value) as answerSum FROM answer " +
            "INNER JOIN survey survey ON answer.survey_id = survey.id " +
            "INNER JOIN user user ON survey.user_id = user.id WHERE user.realm_id = ?1 AND survey.date >= ?2 AND survey.date <= ?3 GROUP BY userId",nativeQuery = true)
    Slice<IUserDTO> getModQuestionsByRealmIdAndBetweenDatesPage(Integer realmId, LocalDate startDate, LocalDate endDate, Pageable pageable);

//    @Query( value = "SELECT new com.kundnojd.DTO.ModQuestionDTO(languageQuestion.question.id, languageQuestion.text, COUNT(survey.id), AVG(answer.value)) " +
//            "FROM Answer answer \n" +
//            "INNER JOIN LanguageQuestion languageQuestion ON answer.question.id = languageQuestion.question.id\n" +
//            "INNER JOIN Survey survey ON answer.survey.id = survey.id \n" +
//            "WHERE languageQuestion.language.id = ?1 AND survey.date >= ?2 AND survey.date <= ?3 AND survey.user.id = ?4 " +
//            "GROUP BY languageQuestion.text, languageQuestion.question.id")
//    Slice<ModQuestionDTO> getAllModQuestionsByLanguageIdAndDatesBetweenAndUserId(Integer languageId, LocalDate startDate, LocalDate endDate, Integer userId, Pageable pageable);

@Query( value = "SELECT languageQuestion.question_id as questionId, languageQuestion.text as text, answer.text as answerText, COUNT(survey.id) as surveyAmount, AVG(answer.value) as answerValue " +
        "FROM answer answer \n" +
        "INNER JOIN language_question languageQuestion ON answer.question_id = languageQuestion.question_id\n" +
        "INNER JOIN survey survey ON answer.survey_id = survey.id \n" +
        "WHERE languageQuestion.language_id = ?1 AND survey.date >= ?2 AND survey.date <= ?3 AND survey.user_id = ?4 " +
        "GROUP BY text, questionId, answerText",nativeQuery = true)
Slice<IModQuestionDTO> getAllModQuestionsByLanguageIdAndDatesBetweenAndUserId(Integer languageId, LocalDate startDate, LocalDate endDate, Integer userId, Pageable pageable);


    @Query( value = "SELECT new com.kundnojd.DTO.ChildCalenderDTO(function('dayofweek', survey.date), SUM(CASE WHEN (survey.answer = 2) THEN 1 ELSE 0 END), " +
            "SUM(CASE WHEN (survey.answer = 1) THEN 1 ELSE 0 END), AVG(answer.value), COUNT(DISTINCT survey.id))\n" +
            "FROM Survey survey\n" +
            "INNER JOIN Answer answer ON survey.id = answer.survey.id\n"+
            "WHERE survey.date >= ?1 AND survey.date <= ?2 AND survey.user.id = ?3\n" +
            "GROUP BY function('dayofweek', survey.date)")
    List<ChildCalenderDTO> getAllChildCalenderDTOBetweenDatesAndUserIdWEEK(LocalDate startDate, LocalDate endDate, Integer userId);

    @Query( value = "SELECT new com.kundnojd.DTO.ChildCalenderDTO(function('week', survey.date), SUM(CASE WHEN (survey.answer = 2) THEN 1 ELSE 0 END), " +
            "SUM(CASE WHEN (survey.answer = 1) THEN 1 ELSE 0 END), AVG(answer.value), COUNT(DISTINCT survey.id))\n" +
            "FROM Survey survey\n" +
            "INNER JOIN Answer answer ON survey.id = answer.survey.id\n"+
            "WHERE survey.date >= ?1 AND survey.date <= ?2 AND survey.user.id = ?3\n" +
            "GROUP BY function('week', survey.date)")
    List<ChildCalenderDTO> getAllChildCalenderDTOBetweenDatesAndUserIdMONTH(LocalDate startDate, LocalDate endDate, Integer userId);

    @Query( value = "SELECT new com.kundnojd.DTO.ChildCalenderDTO(function('month', survey.date), SUM(CASE WHEN (survey.answer = 2) THEN 1 ELSE 0 END)" +
            ", SUM(CASE WHEN (survey.answer = 1) THEN 1 ELSE 0 END), AVG(answer.value), COUNT(DISTINCT survey.id))\n" +
            "FROM Survey survey\n" +
            "INNER JOIN Answer answer ON survey.id = answer.survey.id\n"+
            "WHERE survey.date >= ?1 AND survey.date <= ?2 AND survey.user.id = ?3\n" +
            "GROUP BY function('month', survey.date)")
    List<ChildCalenderDTO> getAllChildCalenderDTOBetweenDatesAndUserIdYEAR(LocalDate startDate, LocalDate endDate, Integer userId);

//    @Query( value = "SELECT new com.kundnojd.DTO.QuestionDTO(answer.question.id, language_question.text, answer.value) FROM Answer answer " +
//            "INNER JOIN LanguageQuestion language_question ON answer.question.id = language_question.question.id " +
//            "WHERE answer.survey.id = ?1 AND language_question.language.id = ?2")
//    List<QuestionDTO> getAllQuestionBuSurveyIdAndLanguageId(Integer surveyId, Integer languageId);

    @Query( value = "SELECT answer.question_id as questionId, language_question.text as text, answer.value as answerValue FROM  answer " +
            "INNER JOIN language_question language_question ON answer.question_id = language_question.question_id " +
            "WHERE answer.survey_id = ?1 AND language_question.language_id = ?2", nativeQuery = true)
    List<IQuestionDTO> getAllQuestionBySurveyIdAndLanguageId(Integer surveyId, Integer languageId);

    @Query( value = "SELECT l_q.question_id as questionId, l_q.text as text \n" +
            "FROM language_question l_q\n" +
            "INNER JOIN question_role q_r ON l_q.question_id = q_r.question_id\n" +
            "INNER JOIN role r ON q_r.role_id = ?1\n" +
            "WHERE l_q.language_id = ?2\n" +
            "GROUP BY questionId, text", nativeQuery = true)
    List<ITranslatedQuestionDTO> getAllQuestionsByRoleIdAndLanguageId(Integer roleId, Integer languageId);

    @Query(value = "SELECT AVG(value) FROM answer WHERE survey_id = ?1", nativeQuery = true)
    Optional<Double> getAverageAnswerBySurveyId(Integer surveyId);

    @Query(value = "SELECT new com.kundnojd.DTO.ParentCalenderDTO(AVG(answer.value), SUM(answer.value)) FROM Answer answer WHERE answer.survey.id IN \n" +
            "(SELECT survey.id FROM Survey survey WHERE survey.user.id = ?1 \n" +
            "AND survey.date >= ?2 AND survey.date <= ?3)")
    Optional<ParentCalenderDTO> getAvgAnswerAndTotalSumByUserIdAndBetweenDates(Integer userId, LocalDate startDate, LocalDate endDate);

    @Query(value = "SELECT (SUM(CASE WHEN (survey.answer = 2) THEN 1 ELSE 0 END) / COUNT(survey.id)) * 100 \n" +
            "FROM survey WHERE survey.user_id = ?1 \n" +
            "AND survey.date >= ?2 AND survey.date <= ?3", nativeQuery = true)
    Optional<Double> getAverageResponseRateByUserIdAndBetweenDates(Integer userId, LocalDate startDate, LocalDate endDate);

    @Query(value = "SELECT user_id as userId, AVG(value) as average, weekofyear(date) as number FROM normalized_data \n" +
            "WHERE user_id IN ?1 AND date >= ?2 AND date <= ?3 \n" +
            "GROUP BY user_id, number", nativeQuery = true)
    List<IUserAvgPeriod> getAverageScoreWeekByUsersAndBetweenDates(List<Integer> userIds, LocalDate startDate, LocalDate endDate);

    @Query(value = "SELECT user_id as userId, AVG(value) as average, dayofweek(date) as number FROM normalized_data \n" +
            "WHERE user_id IN ?1 AND date >= ?2 AND date <= ?3 \n" +
            "GROUP BY user_id, number", nativeQuery = true)
    List<IUserAvgPeriod> getAverageScoreDayByUsersAndBetweenDates(List<Integer> userIds, LocalDate startDate, LocalDate endDate);

    @Query(value = "SELECT user_id as userId, AVG(value) as average, month(date) as number FROM normalized_data \n" +
            "WHERE user_id IN ?1 AND date >= ?2 AND date <= ?3 \n" +
            "GROUP BY userId, number", nativeQuery = true)
    List<IUserAvgPeriod> getAverageScoreMonthByUsersAndBetweenDates(List<Integer> userIds, LocalDate startDate, LocalDate endDate);

    @Query(value = "SELECT user_id as userId, AVG(value) as average, year(date) as number FROM normalized_data \n" +
            "WHERE user_id IN ?1 AND date >= ?2 AND date <= ?3 \n" +
            "GROUP BY userId, number", nativeQuery = true)
    List<IUserAvgPeriod> getAverageScoreYearByUsersAndBetweenDates(List<Integer> userIds, LocalDate startDate, LocalDate endDate);
}
