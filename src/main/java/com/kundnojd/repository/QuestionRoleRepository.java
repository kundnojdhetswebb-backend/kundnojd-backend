package com.kundnojd.repository;

import com.kundnojd.model.QuestionRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionRoleRepository extends JpaRepository<QuestionRole, Integer> {
}
