package com.kundnojd.repository;

import com.kundnojd.model.Realm;
import com.kundnojd.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User,Integer> {
    List<User> findAllByRealm(Realm realm);
}
