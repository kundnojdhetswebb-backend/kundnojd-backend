package com.kundnojd.repository;

import com.kundnojd.model.Realm;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RealmRepository extends JpaRepository<Realm, Integer> {
    //@Query(value = "SELECT new com.kundnojd.DTO.TechnicianDTO(technician.id, COUNT(DISTINCT survey.Id), AVG(score.value), SUM(score.value)) FROM Score score INNER JOIN Survey survey ON score.survey.Id = survey.Id INNER JOIN Technician technician ON survey.technician.id = technician.id WHERE technician.realm.id = ?1 AND survey.date >= ?2 AND survey.date <= ?3 GROUP BY technician.id")
    //Slice<TechnicianDTO> getModQuestionsByRealmIdAndBetweenDatesPage(Integer realmId, LocalDate startDate, LocalDate endDate, Pageable pageable);

    //@Query(value = "SELECT new com.kundnojd.DTO.TechnicianDTO(technician.id, COUNT(DISTINCT survey.Id), AVG(score.value), SUM(score.value)) FROM Realm realm INNER JOIN Technician technician ON technician.realm.id = ?1 INNER JOIN Survey survey ON technician.id = survey.technician.id WHERE survey.date >= ?2 AND survey.date <= ?3 GROUP BY technician.id")
    //Slice<TechnicianDTO> getModQuestionsByRealmIdAndBetweenDatesPage(Integer realmId, LocalDate startDate, LocalDate endDate, Pageable pageable);
}
