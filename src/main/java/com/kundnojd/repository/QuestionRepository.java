package com.kundnojd.repository;

import com.kundnojd.DTO.AllQuestionDTO;
import com.kundnojd.DTO.QuestionDTO;
import com.kundnojd.model.Question;
import com.kundnojd.model.Realm;
import com.kundnojd.repository.IDTO.IQuestionDTO;
import com.kundnojd.repository.IDTO.ITranslatedQuestionDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.*;

@Repository
public interface QuestionRepository extends JpaRepository<Question,Integer> {
//    List<Question> findAllByRealmAndIsActive(Realm realm, Boolean isActive);

    @Query( value = "SELECT id FROM question WHERE id IN (SELECT question_id FROM answer WHERE survey_id IN " +
            "(SELECT id FROM survey WHERE user_id = ?1 AND date >= ?2 AND date <= ?3))", nativeQuery = true)
    List<Integer> getAllQuestionIdsByTechnicianIdAndDates(Integer technicianId, LocalDate startDate, LocalDate endDate);

//    @Query( value = "SELECT new com.kundnojd.DTO.QuestionDTO(question.id, language_question.text) FROM Question question \n" +
//            "INNER JOIN LanguageQuestion language_question ON language_question.question.id = question.id\n" +
//            "INNER JOIN Language language ON language_question.language.id = language.id\n" +
//            "WHERE language_question.question.id = question.id\n" +
//            "AND question.realm.id = ?1 AND language_question.language.id = ?2\n" +
//            "AND question.isActive = ?3")
//    List<QuestionDTO> getAllQuestionDTOByRealmIdAndLanguageIdAndQuestionIs(Integer realmId, Integer languageId, Boolean isActive);

    @Query( value = "SELECT new com.kundnojd.DTO.QuestionDTO(question.id, language_question.text) FROM Question question \n" +
            "INNER JOIN LanguageQuestion language_question ON language_question.question.id = question.id\n" +
            "INNER JOIN Language language ON language_question.language.id = language.id\n" +
            "WHERE language_question.question.id = question.id\n" +
            "AND question.realm.id = ?1 AND language_question.language.id = ?2\n")
    List<QuestionDTO> getAllQuestionDTOByRealmIdAndLanguageId(Integer realmId, Integer languageId);

    @Query( value = "SELECT l_q.question_id as questionId, l_q.text FROM language_question l_q\n" +
            "INNER JOIN question q ON q.id = l_q.question_id\n" +
            "WHERE l_q.language_id = ?1 AND  q.realm_id = ?2\n" +
            "GROUP BY l_q.question_id, l_q.text", nativeQuery = true)
    List<ITranslatedQuestionDTO> getALlTranslatedQuestionsByLanguageIdAndRealmId(Integer languageId, Integer realmId);

    @Query( value = "SELECT l_q.question_id as questionId FROM language_question l_q\n" +
            "INNER JOIN question q ON q.id = l_q.question_id\n" +
            "WHERE l_q.language_id != ?1 AND  q.realm_id = ?2\n" +
            "GROUP BY questionId", nativeQuery = true)
    List<IQuestionDTO> getNotTranslatedQuestionsByLanguageIdAndRealmId(Integer languageId, Integer realmId);

    @Query( value = "SELECT q.id as questionId FROM language_question l_q\n" +
            "INNER JOIN question q ON q.id = l_q.question_id\n" +
            "WHERE l_q.language_id != ?1 AND  q.realm_id = ?2\n" +
            "AND q.id NOT IN ?3\n" +
            "GROUP BY questionId", nativeQuery = true)
    List<IQuestionDTO> getNotTranslatedQuestionsByLanguageIdAndRealmIdAndNotInListId(Integer languageId, Integer realmId, List<Integer> questionIds);

    @Query( value = "SELECT l_q.question_id as questionId, l_q.text FROM language_question l_q\n" +
            "INNER JOIN question q ON q.id = l_q.question_id\n" +
            "INNER JOIN question_role q_r ON q_r.question_id = q.id\n" +
            "WHERE l_q.language_id = ?1 AND  q.realm_id = ?2 AND q_r.id = ?3\n" +
            "GROUP BY l_q.question_id, l_q.text\n", nativeQuery = true)
    List<ITranslatedQuestionDTO> getAllTranslatedQuestionsByLanguageIdRealmIdRoleId(Integer languageId, Integer realmId, Integer roleId);

    @Query (value = "SELECT l_q.question_id as questionId FROM language_question l_q\n" +
            "INNER JOIN question q ON q.id = l_q.question_id\n" +
            "INNER JOIN question_role q_r ON q_r.question_id = q.id\n" +
            "WHERE l_q.language_id != 1 AND  q.realm_id = 1 AND q_r.role_id = 1\n" +
            "GROUP BY l_q.question_id", nativeQuery = true)
    List<IQuestionDTO> getAllNotTranslatedQuestionsByLanguageIdRealmIdRoleId(Integer languageId, Integer realmId, Integer roleId);

//    @Query( value = "SELECT new com.kundnojd.DTO.AllQuestionDTO(language_question.question.id, language_question.text)\n" +
//            "FROM Question question\n" +
//            "INNER JOIN LanguageQuestion language_question ON language_question.question.id = question.id\n" +
//            "WHERE question.realm.id = ?1 AND language_question.language.id = ?2 ORDER BY question.isActive desc")
//    List<AllQuestionDTO> getAllQuestionDTOsByLanguageIdAndRealmId(Integer realmId, Integer languageId);

//    @Query(value = "SELECT new com.kundnojd.DTO.AllQuestionDTO(question.id) FROM Question question " +
//            "INNER JOIN LanguageQuestion language_question ON language_question.question.id = question.id\n" +
//            "WHERE question.realm.id = ?1 AND language_question.question.id NOT IN " +
//            "(SELECT l2.question.id FROM language_question l2 WHERE l2.language.id = ?2)\n" +
//            "GROUP BY language_question.question.id\n")
//    List<AllQuestionDTO> notTranslatedQuestions(Integer realmId, Integer languageId);
}
