package com.kundnojd.repository;

import com.kundnojd.model.Language;
import com.kundnojd.model.LanguageQuestion;
import com.kundnojd.model.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository

public interface LanguageQuestionRepository extends JpaRepository<LanguageQuestion, Integer> {
    Optional<LanguageQuestion> findByQuestionAndLanguage(Question question, Language language);
    List<LanguageQuestion> findAllByQuestion(Question question);

    @Query( value = "SELECT * FROM language_question WHERE language_id = ?1 AND question_id IN " +
            "(SELECT id FROM question WHERE id IN (SELECT question_id FROM score WHERE survey_id IN " +
            "(SELECT id FROM survey WHERE user_id = ?2 AND date >= ?3 AND date <= ?4)))", nativeQuery = true)
    List<LanguageQuestion> getAllByLanguageIdAndTechnicianIdAndBetweenDates(Integer languageId, Integer userId, LocalDate startDate, LocalDate endDate);

}
