package com.kundnojd.enums;

import java.time.LocalDate;

public enum CalendarTypeEnum {
    DAY, WEEK, MONTH, YEAR
}
