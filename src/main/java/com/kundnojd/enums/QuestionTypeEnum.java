package com.kundnojd.enums;

public enum QuestionTypeEnum {
    QUANTITATIVE, QUALITATIVE, HYBRID
}