package com.kundnojd.model;

import org.springframework.web.bind.annotation.GetMapping;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Moderator implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private String password;

    @Column
    private String email;

    @Column
    private Boolean isAdmin;

    @ManyToOne
    @JoinColumn(name = "realm_id", nullable = true)
    private Realm realm;

    public Moderator(){}
    public Moderator(String password, String email, Boolean isAdmin, Realm realm) {
        this.password = password;
        this.email = email;
        this.isAdmin = isAdmin;
        this.realm = realm;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getAdmin() {
        return isAdmin;
    }

    public void setAdmin(Boolean admin) {
        isAdmin = admin;
    }

    public Realm getRealm() {
        return realm;
    }

    public void setRealm(Realm realm) {
        this.realm = realm;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
