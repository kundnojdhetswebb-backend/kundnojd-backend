package com.kundnojd.model;

import com.kundnojd.enums.AnswerEnum;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
public class Survey implements Serializable {
    
    @Id
    //@GeneratedValue (strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
//    @Column(columnDefinition = "boolean default false")
    private AnswerEnum answer;

    @Column
    private LocalDate date;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "participant_id")
    private Participant participant;

    public Survey(Integer id, Integer customerId, User user) {
        this.id = id;
        this.user = user;
    }

    public Survey(AnswerEnum answer, LocalDate date, User user) {
        this.answer = answer;
        this.date = date;
        this.user = user;
    }
    public Survey(Integer id, User user, Integer customerId, AnswerEnum answerEnum) {
        this.id = id;
        this.user = user;
        this.answer = answerEnum;
        this.date = LocalDate.now();
    }

    public Survey(User user, Integer customerId) {
        this.user = user;
    }

    public Survey(Integer id, AnswerEnum answer, LocalDate date, Integer customerId, User user) {
        this.id = id;
        this.answer = answer;
        this.date = date;
        this.user = user;
    }

    public Survey(User user) {
        this.user = user;
    }

    public Survey() {
    }

    public Survey(User user, Integer customerId, AnswerEnum answerEnum) {
        this.user = user;
        this.answer = answerEnum;
        this.date = LocalDate.now();
    }

    public Survey(Integer survey_id, User user, Integer customerId) {
        this.id = survey_id;
        this.user = user;
    }

    public Survey(Integer id, AnswerEnum answer, LocalDate date, User user, Participant participant) {
        this.id = id;
        this.answer = answer;
        this.date = date;
        this.user = user;
        this.participant = participant;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public AnswerEnum getAnswer() {
        return answer;
    }

    public void setAnswer(AnswerEnum answer) {
        this.answer = answer;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Participant getParticipant() {
        return participant;
    }

    public void setParticipant(Participant participant) {
        this.participant = participant;
    }
}
