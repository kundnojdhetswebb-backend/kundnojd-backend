package com.kundnojd.model;

public class CommonResponse {
    public Object data;
    public String message;

    public CommonResponse(Object data, String message) {
        this.data = data;
        this.message = message;
    }

    public CommonResponse() {
    }

    public CommonResponse(String message) {
        this.message = message;
    }
}


