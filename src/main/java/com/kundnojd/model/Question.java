package com.kundnojd.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.kundnojd.enums.QuestionTypeEnum;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Question implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Integer id;

    @Column
    private QuestionTypeEnum questionType;

    @ManyToOne
    @JoinColumn(name = "realm_id", nullable = true)
    private Realm realm;

    public Question() {
    }

    public Question(Boolean isActive, Realm realm) {
//        this.isActive = isActive;
        this.realm = realm;
    }

    public Question(Boolean isActive, QuestionTypeEnum questionType, Realm realm) {
//        this.isActive = isActive;
        this.questionType = questionType;
        this.realm = realm;
    }

    public Question(String text, Boolean isActive) {
//        this.isActive = isActive;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

//    public Boolean getActive() {
//        return isActive;
//    }
//
//    public void setActive(Boolean active) {
//        isActive = active;
//    }

    public QuestionTypeEnum getQuestionType() {
        return questionType;
    }

    public void setQuestionType(QuestionTypeEnum questionType) {
        this.questionType = questionType;
    }

    public Realm getRealm() {
        return realm;
    }

    public void setRealm(Realm realm) {
        this.realm = realm;
    }

}
