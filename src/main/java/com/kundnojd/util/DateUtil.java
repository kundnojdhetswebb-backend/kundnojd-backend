package com.kundnojd.util;

import com.kundnojd.DTO.QueryDTO;
import com.kundnojd.enums.TimeSpanEnum;
import com.kundnojd.services.AnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Optional;



@Component
public class DateUtil {
    @Autowired
    AnswerService answerService;

    public TimeSpanEnum getTimeSpan(Optional<String> timeSpan){
       return timeSpan.isPresent() ? TimeSpanEnum.valueOf(timeSpan.get()) : TimeSpanEnum.THIS_WEEK;
    }

    public QueryDTO getStartAndEndDate(Optional<String> startDate, Optional<String> endDate, Optional<String> timeSpan){
       return startDate.isPresent() && endDate.isPresent() && timeSpan.isEmpty() ? new QueryDTO(LocalDate.parse(startDate.get()), LocalDate.parse(endDate.get())) :
                answerService.getTimeSpan(getTimeSpan(timeSpan));

    }
}
