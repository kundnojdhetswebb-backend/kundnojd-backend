package com.kundnojd.util;

import com.kundnojd.DTO.PageDTO;
import com.kundnojd.model.CommonResponse;
import org.springframework.data.domain.Slice;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class MessageUtil {
    public ResponseEntity responseEntity(Object data, String message, HttpStatus status){
        return new ResponseEntity(new CommonResponse(data, message), status);
    }
    public ResponseEntity responseEntity(String message, HttpStatus status){
        return new ResponseEntity(new CommonResponse(message), status);
    }
    public ResponseEntity pageResponseEntityOK(Slice s, String message){
        return new ResponseEntity(new CommonResponse(new PageDTO(s.getContent(), s.hasPrevious(), s.hasNext()), message), HttpStatus.OK);
    }
}
