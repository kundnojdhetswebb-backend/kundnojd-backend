FROM maven:3.6.3-jdk-11 as build
COPY src /usr/src/app/src
COPY pom.xml /usr/src/app
RUN mvn -q -f /usr/src/app/pom.xml clean package

FROM openjdk:11
COPY --from=build /usr/src/app/target/*.jar /usr/app/kundnojd.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/usr/app/kundnojd.jar"]
